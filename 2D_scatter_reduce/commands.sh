#!/usr/bin/env bash

set -e

#Pw has to be a power of two and is the number of PEs per dimension
Pw=4
#B has to be a power of 4 and is the Vector size
B=16
x_dim=$((7+$Pw))
y_dim=$((2+$Pw))


cslc ./layout.csl --fabric-dims=$x_dim,$y_dim \
--fabric-offsets=4,1 --params=Pw:$Pw,B:$B -o out --memcpy --channels 1

cs_python run.py --name out