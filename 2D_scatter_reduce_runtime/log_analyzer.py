import matplotlib.pyplot as plt


identifier = "at exit"

x_values = []
y_values = []

# Open the file and read the coordinates
coordinates = []
with open('sim.log', 'r') as file:
    for line in file:
        pe = line.split()[1]
        if line.find(identifier) != -1:
            x, y = pe.strip('PE():').split(',')
            coordinates.append((x, y))
            x_values.append(int(x))
            y_values.append(int(y))

# Set up the plot with a coordinate system
plt.figure()
plt.xlabel('X')
plt.ylabel('Y')
plt.title('PEs that satisfy:'+ identifier)

# Define the range of the coordinate system based on the points
plt.xlim(min(x_values) - 1, max(x_values) + 1)
plt.ylim(min(y_values) - 1, max(y_values) + 1)

# Plot the points within the coordinate system
plt.scatter(x_values, y_values)
plt.grid(True)
plt.gca().invert_yaxis()
plt.show()
