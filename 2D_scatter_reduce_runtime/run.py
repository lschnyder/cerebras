#!/usr/bin/env cs_python

import argparse
import json
import numpy as np
from cerebras.sdk.runtime.sdkruntimepybind import SdkRuntime, MemcpyDataType, MemcpyOrder # pylint: disable=no-name-in-module
import sys
np.set_printoptions(threshold=sys.maxsize)
# Read arguments
parser = argparse.ArgumentParser()
parser.add_argument('--name', help="the test compile output dir")
parser.add_argument('--cmaddr', help="IP:port for CS system")
args = parser.parse_args()

# Get matrix dimensions from compile metadata
with open(f"{args.name}/out.json", encoding='utf-8') as json_file:
  compile_data = json.load(json_file)

B = int(compile_data['params']['B'])
Pw = int(compile_data['params']['Pw'])

inputs = np.arange(1, Pw*Pw*B + 1, dtype=np.float32)
# inputs = np.ones(Pw*Pw*B, dtype=np.float32)


#WARNING: this only works if B >= Pw*Pw
# assert(B >= Pw*Pw)
# expected = inputs.copy();
# expected.reshape((Pw,Pw,B))
# expected.sum

runner = SdkRuntime(args.name, cmaddr=args.cmaddr)

b_symbol = runner.get_id('b')
sr_res_symbol = runner.get_id('sr_res')

print("B =", B, " P =", Pw*Pw)

print("Starting simulation...")

runner.load()
runner.run()

print("simulation successfully started!")
print("copying data to device...")

runner.memcpy_h2d(b_symbol, inputs, 0, 0, Pw, Pw, B, streaming=False,
                  order=MemcpyOrder.ROW_MAJOR, data_type=MemcpyDataType.MEMCPY_32BIT, nonblock=False)

print("data successfully copied!")
print("launching...")

runner.launch('execute', nonblock=False)

print("successfully launched!")
# print("copying data back...")

# sr_res = np.zeros([Pw*Pw*(max(B//(Pw*Pw),1))], dtype=np.float32)
# runner.memcpy_d2h(sr_res, sr_res_symbol, 0, 0, Pw, Pw, max(B//(Pw*Pw),1), streaming=False,
#                   order=MemcpyOrder.ROW_MAJOR, data_type=MemcpyDataType.MEMCPY_32BIT, nonblock=False)

# print("data successfully copied!")

runner.stop()

# print(sr_res.reshape((Pw*Pw,max(B//(Pw*Pw),1))))

# np.testing.assert_allclose(inputs, res)
# print("Brother, I think it worked!")