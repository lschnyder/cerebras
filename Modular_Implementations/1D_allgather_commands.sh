#!/usr/bin/env bash

set -e

for num_pes in {2..2}
do
    for vec_len in {100..100}
    do
        x_dim=$((7+$num_pes))

        cslc ./1D_allgather_layout.csl --fabric-dims=$x_dim,3 \
        --fabric-offsets=4,1 --params=Pw:$num_pes,B:$vec_len,algo:0 -o out --memcpy --channels 1
        cs_python 1D_allgather_run.py --name out
    done
done