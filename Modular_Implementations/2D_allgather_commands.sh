#!/usr/bin/env bash

set -e

#Algo 0:ring
#     1:doublestar
#     2:naive

for Algo in {1..1}
do
    for vec_len in {1..1}
    do
        for num_pes_x in {3..3}
        do
            for num_pes_y in {1..1}
            do
                x_dim=$((7+$num_pes_x))
                y_dim=$((2+$num_pes_y))

                cslc ./2D_allgather_layout.csl --fabric-dims=$x_dim,$y_dim \
                --fabric-offsets=4,1 --params=Pw:$num_pes_x,Ph:$num_pes_y,B:$vec_len,algo:$Algo -o out --memcpy --channels 1
                cs_python 2D_allgather_run.py --name out
            done
        done
    done
done