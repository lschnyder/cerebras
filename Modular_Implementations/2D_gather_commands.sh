#!/usr/bin/env bash

#we're very close to 4032, ie sequentially writing the own vector + contention
# 3984 is the minimum contention, ie a true lower bound

# With dsds:
# 12x7x48 takes ca. 4147 opt>=3984 (gather start - gather end)

# With dsrs:
# 12x7x48 takes ca. 4133 opt>=3984 (gather start - gather end)
# 10x10x50 takes ca. 5107 opt>=5000 (gather start - gather end)

set -e

for num_pes_x in {10..10}
do
    for num_pes_y in {10..10}
    do
        for vec_len in {50..50}
        do
            x_dim=$((7+$num_pes_x))
            y_dim=$((2+$num_pes_y))

            cslc ./2D_gather_layout.csl --fabric-dims=$x_dim,$y_dim \
            --fabric-offsets=4,1 --params=Pw:$num_pes_x,Ph:$num_pes_y,B:$vec_len,algo:0 -o out --memcpy --channels 1
            cs_python 2D_gather_run.py --name out
        done
    done
done