#!/usr/bin/env bash

set -e

#num_pes_x must be a power of 2
for num_pes_x in {64..64}
do
    #vec_len must be a power of 4
    for vec_len in {4096..4096}
    do
        x_dim=$((7+$num_pes_x))
        y_dim=$((2+$num_pes_x))

        cslc ./2D_reduce_layout.csl --fabric-dims=$x_dim,$y_dim \
        --fabric-offsets=4,1 --params=Pw:$num_pes_x,B:$vec_len,algo:0 -o out --memcpy --channels 1
        cs_python 2D_reduce_run.py --name out
    done
done