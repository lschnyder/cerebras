#!/usr/bin/env cs_python

import argparse
import json
import numpy as np
from cerebras.sdk.runtime.sdkruntimepybind import SdkRuntime, MemcpyDataType, MemcpyOrder # pylint: disable=no-name-in-module

# Read arguments
parser = argparse.ArgumentParser()
parser.add_argument('--name', help="the test compile output dir")
parser.add_argument('--cmaddr', help="IP:port for CS system")
args = parser.parse_args()

# Get matrix dimensions from compile metadata
with open(f"{args.name}/out.json", encoding='utf-8') as json_file:
  compile_data = json.load(json_file)

Pw = int(compile_data['params']['Pw'])
B = int(compile_data['params']['B'])

inputs = np.arange(1, B*Pw*Pw + 1, dtype=np.float32)

# expected = inputs.copy().reshape((B,Pw,Ph))
# for i in range(Pw):
#   if i % 2 == 1:
#     expected[i] = np.flip(expected[i], 0);
            
# expected = expected.flatten()
runner = SdkRuntime(args.name, cmaddr=args.cmaddr)

b_symbol = runner.get_id('b')

print("B=", B, " Pw=", Pw)

print("Starting simulation...")

runner.load()
runner.run()

print("simulation successfully started!")
# print("copying data to device...")

# runner.memcpy_h2d(b_symbol, inputs, 0, 0, Pw, Pw, B, streaming=False,
#                   order=MemcpyOrder.ROW_MAJOR, data_type=MemcpyDataType.MEMCPY_32BIT, nonblock=False)

# print("data successfully copied!")
print("launching...")

runner.launch('f_sync_and_execute', nonblock=False)

print("successfully launched!")
print("copying data back...")

# res = np.zeros([B], dtype=np.float32)
# runner.memcpy_d2h(res, b_symbol, 0, 0, 1, 1, B, streaming=False,
#                   order=MemcpyOrder.ROW_MAJOR, data_type=MemcpyDataType.MEMCPY_32BIT, nonblock=False)

# print("data successfully copied!")

runner.stop()

# print(res)

# np.testing.assert_allclose(res, expected.flatten())
# for i in range(Pw):
#   res_i = res[i*Pw*B:(i+1)*Pw*B]
# print("Brother, I think it worked!")