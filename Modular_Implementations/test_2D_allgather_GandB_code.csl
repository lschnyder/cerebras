const layout_module = @import_module("<layout>");
param memcpy_params: comptime_struct;
param B: u16;
param Pw: u16;
param Ph: u16;
param algo: u16;

param local_task_id_0 : local_task_id;
param local_task_id_1 : local_task_id;
param local_task_id_2 : local_task_id;
param exit_task_id: local_task_id;
param bcast_right_id: local_task_id;
param finish_sync_id: local_task_id;

param color_0 : color;
param color_1 : color;
param color_2 : color;
param color_3 : color;
param color_4 : color;
param color_5 : color;
param color_6 : color;

const prt = @import_module("<simprint>");
const sys_mod = @import_module("<memcpy/memcpy>", memcpy_params);
const timestamp = @import_module("<time>");

const reduce_2d = @import_module("external/2d_chain_runtime.csl", .{.color_1 = color_4, .color_2 = color_5, .M = Ph, .N = Pw, .timestamp = timestamp});
const broadcast_2d = @import_module("external/2d_broadcast_runtime.csl", .{.broadcast_color = color_6, .M = Ph, .N = Pw, .timestamp = timestamp, .root_x = 0, .root_y = 0});

const gather_2d = @import_module("modules/2D_gather_simple.csl", .{.green = @get_color(13), .Pw=Pw, .Ph=Ph, .timestamp = timestamp});

fn simprint_pe_coords() void {
  prt.print_string("PE(");
  prt.print_u16_decimal(pe_id_x);
  prt.print_string(",");
  prt.print_u16_decimal(pe_id_y);
  prt.print_string("): ");
}

var pe_id_x : u16;
var pe_id_y : u16;

// tsc_size_words = 3
// starting time of H2D/D2H
var tscStartBuffer = @zeros([timestamp.tsc_size_words]u16);
// ending time of H2D/D2H
var tscEndBuffer = @zeros([timestamp.tsc_size_words]u16);
var tscRefBuffer = @zeros([timestamp.tsc_size_words]u16);

var time_buf_u16 = @zeros([timestamp.tsc_size_words*2]u16);
var time_ref_u16 = @zeros([timestamp.tsc_size_words]u16);

var ptr_time_buf_u16: [*]u16 = &time_buf_u16;
var ptr_time_ref: [*]u16 = &time_ref_u16;

var mem = @constants([8192]f32, @as(f32, 1.0));
var mem_ptr: [*]f32 = &mem;

var dummy = @zeros([2048]f32);
const dummy_mem_dsd = @get_dsd(mem1d_dsd, .{ .tensor_access = |i|{2048} -> dummy[i]});
var xd = @zeros([@as(u16, 2.0*@as(f32, (Ph + Pw)))]f32);
var xd_mem_dsd = @get_dsd(mem1d_dsd, .{ .tensor_access = |i|{1} -> xd[i]});
var warmup_iter: u16 = 0;

fn f_sync(n: i16) void {
  timestamp.enable_tsc();
  var id_x = layout_module.get_x_coord();
  var id_y = layout_module.get_y_coord();
  pe_id_x = id_x;
  pe_id_y = id_y;
  // best so far is: 1.334x, 1.334y
  //xd_mem_dsd = @set_dsd_length(xd_mem_dsd, @as(u16, 1.334*@as(f32, (Ph - id_y)) + 1.334*@as(f32, (Pw - id_x))));
  xd_mem_dsd = @set_dsd_length(xd_mem_dsd, @as(u16, 2.0*@as(f32, (Ph + Pw - id_y - id_x))));
  reduce_2d.setup();
  broadcast_2d.setup();

  gather_2d.configure_network(id_x, id_y);
  
  // Perform chain reduce to synchronize all PEs
  @fadds(dummy_mem_dsd, dummy_mem_dsd, 1.0);
  @fadds(dummy_mem_dsd, dummy_mem_dsd, 1.0);
  reduce_2d.transfer_data(&dummy, 2048, bcast_right_id, &tscEndBuffer);
}

task bcast_right() void {
  if (warmup_iter == 0) {
    warmup_iter += 1;
    broadcast_2d.transfer_data(&dummy, 2048, bcast_right_id, &tscEndBuffer);
  } else if (warmup_iter == 1) {
    warmup_iter += 1;
    broadcast_2d.transfer_data(&dummy, 2048, bcast_right_id, &tscEndBuffer);
  } else if (warmup_iter == 2) {
    warmup_iter += 1;
    reduce_2d.transfer_data(&dummy, 2048, bcast_right_id, &tscEndBuffer);
  } else if (warmup_iter == 3) {
    broadcast_2d.transfer_data(&dummy, 2048, finish_sync_id, &tscEndBuffer);
  }
  
}

task finish_sync() void {
  timestamp.get_timestamp(&tscRefBuffer);
  delay();
  compute();
}

// Function to delay the start of compute by some number of cycles in order for all PEs to start at a relatively similar time
fn delay() void {
  @fmovs(xd_mem_dsd, 1.0);
}

fn compute() void {
  gather_2d.transfer_data(&dummy, mem_ptr, B, exit_task_id, &tscStartBuffer, &tscEndBuffer);
}

var i : u16 = 0;
task exit_task() void {
  if (i == 0){
    i += 1;
    broadcast_2d.transfer_data(mem_ptr, B, exit_task_id, &tscEndBuffer);
  }
  timestamp.get_timestamp(&tscEndBuffer);

  sys_mod.unblock_cmd_stream();
}

fn f_memcpy_timestamps() void {

  time_buf_u16[0] = tscStartBuffer[0];
  time_buf_u16[1] = tscStartBuffer[1];
  time_buf_u16[2] = tscStartBuffer[2];

  time_buf_u16[3] = tscEndBuffer[0];
  time_buf_u16[4] = tscEndBuffer[1];
  time_buf_u16[5] = tscEndBuffer[2];

  time_ref_u16[0] = tscRefBuffer[0];
  time_ref_u16[1] = tscRefBuffer[1];
  time_ref_u16[2] = tscRefBuffer[2];

  sys_mod.unblock_cmd_stream();
}

comptime {
  @bind_local_task(exit_task, exit_task_id);
  @bind_local_task(bcast_right, bcast_right_id);
  @bind_local_task(finish_sync, finish_sync_id);

  broadcast_2d.configure_network();
  reduce_2d.configure_network();

  @export_symbol(mem_ptr, "mem");
  @export_symbol(ptr_time_buf_u16, "time_buf_u16");
  @export_symbol(ptr_time_ref, "time_ref");

  @export_symbol(compute);
  @export_symbol(f_sync);
  @export_symbol(f_memcpy_timestamps);
}
