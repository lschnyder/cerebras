# Spatial Collectives Experiments
Repo with experiments for the spatial collectives paper on Cerebras.

## Experiments to perform
We perform each experiments 5 times.

### 1D
1. 512 PEs and increasing vector length of reduce and allreduce
2. Fixed vector length 256 for increasing number of PEs

### 2D
1. 512 x 512 PEs and increasing vector length of reduce and allreduce
2. Fixed vector length 256 for increasing PE grid size

### Optimal Pre Order Reduce
Same experiments as for 1D

### Broadcast
We report the time to broadcast to the right and then to the left, the result should be halved

1. 512 for increasing vector length
2. Fixed vector length 256 for increasing number of PEs

## How to run the experiments
First to calibrate the synchronisation, run `commands_test.sh` and check in `results.txt`, whether the starting times are not too far apart.

To run 1D experiments run `commands.sh`

To run 2D experiments run `commands_2d.sh`

To run pre order experiments run `commands_pre_order.sh`, might need to import some python libraries

To run broadcast run `commands_broadcast.sh`

