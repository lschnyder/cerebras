``pe.csl`` contains the per-dimension implementation of the collective
communication API. Users should import it per-dimension.
The user has to import ``pe.csl`` with ``dim_params`` which contains
the dimension to operate on, routable colors and entrypoints. Besides
the user can assign two input/output queue IDs (``queues``) and a set
of DSRs (``dest_dsr_ids``, ``src0_dsr_ids``, ``src1_dsr_ids``). If
the user does not assign those parameters, the default values apply.
It is the user's responsibility to assign queue IDs and DSR IDs
properly, i.e. no resource is shared among different ``pe.csl`` modules.

The user is allowed to call an arbitrary sequence of these primitive
operations. Each of these operations will configure the network's
routing accordingly. For example, a broadcast operation requires a single
root PE to send data to all other PEs in its row or column, while a gather
operation requires a single root PE to *receive* data from all other PEs in
its row or column. All of the primitives currently supported have a single
root PE which either sends data (``broadcast``, ``scatter``) or receives the
final result of the collective function (``gather``, ``reduce``).

Every operation may need to re-configure the network
that was configured by the previous operation.
Reconfiguration must happen using teardown wavelets that will be routed using
the *current* network configuration
(this is needed because the existing hardware generations do
not allow a PE to teardown itself - teardowns must always be triggered by
wavelets that are routed into the appropriate PEs).
As a result, we must
maintain global state that indicates the current network configuration.
This state is then used to call the appropriate re-configuration logic.
Such re-configuration might not be needed if the same primitive is called
multiple times in sequence.

With that in mind, here are the logical steps that are followed when a user
calls one of the available primitives:

1. The user calls one of the primitives.

  The call to a primitive will imply a requested network configuration. For
  example,
  ``broadcast`` with a root of ``2`` implies a network with routing going
  from PE2 to all other PEs in the row or column whereas ``gather`` with a root
  of 3 impies a network with routing going from all PEs towards PE3.

  We write the requested network configuration to global state.

2. Initiate teardowns.

  To safely reconfigure each PE's router to the new requested configuration,
  we must send a teardown wavelet to every PE in our row or column. How we
  send these teardown wavelets depends on the current network configuration,
  which we can read from global state.

  For example, if the current network is a ``broadcast`` network rooted at PE2,
  PE2 can send a teardown wavelet to all other PEs. If however the current
  network is a ``gather`` network rooted at PE3, because the routing flows
  inward, the PEs on either end of the row and column need to send teardown
  wavelets.

  Most PEs in the row or column will not need to send out teardown wavelets.
  All PEs need to wait to receive teardown wavelets. Those PEs who were
  responsible for sending teardown wavelets will receive their own teardowns,
  whereas other PEs will receive teardowns sent from other PEs.

3. Reconfiguration.

  Once a PE has received a teardown wavelet, it can reconfigure its router
  from the current network configuration to the new requested network
  configuration. The PE will read the requested network configuration from
  global state, then write the appropriate bit-masks to its configuration
  registers. Once this is done, the PE will activate the task responsible
  for actually transfering data.

4. Transferring data.

  We read information about the current primitive from global state and
  perform DSD operations accordingly. For example, in a ``broadcast`` operation
  rooted at PE2, PE2 will send data out to the fabric while all other PEs will
  read data in from the fabric. Conversely, in a ``gather`` operation,
  non-root PEs will send data out to the fabric while the root reads data
  in from the fabric.

  Sometimes, the data transfer will need to occur in multiple steps.
  For example, in ``gather``, the root PE will first receive data from the
  "negative" direction (``NORTH`` for columns, ``WEST`` for rows), then copy
  its own chunk of the data to the receiving buffer, then finally receive
  data from the opposite "positive" direction.

  Most of these data transfer operations are asynchronous, so they will require
  re-activating the data transfer task upon completion. As a result, this task
  is structured as a state machine.

  Once all data transfers are done, this task will re-activate itself.

5. Callback.

  On the final activation of the data transfer task, we read our current status
  from global state, and determine that all work on the current primitive is
  complete. We then activate the user-specified callback color, returning
  control to the user and indicating that the collective communication
  primitive has successfully executed.

Library Design Notes
--------------------

In this section, we will seek to explain design decisions taken by this library.
More specific documentation is also available as comments in the library code.

1. Getting all PEs into teardown mode

  One of the key challenges in designing this library is tearing down every
  PE in our row or column. As mentioned above, current hardware does not allow
  PEs to enter teardown mode for a particular color without receiving a
  teardown wavelet on that color. This means that for each permitted network
  configuration, we must also have a clear strategy for what PEs should send
  out teardown wavelets for getting us *out* of that network configuration.

  To be more concrete: if our current network configuration is a ``broadcast``
  network rooted at PE1, and our next primitive is ``broadcast(root = 3, ...)``,
  how does PE1 know to broadcast a teardown wavelet?

  Because *every* PE in the row or column must participate in *every*
  collective, we know that PE1 will eventually call
  ``broadcast(root = 3, ...)``.
  The logic inside the ``broadcast`` function will see that PE1 is the current
  root and therefore must send a teardown wavelet to all other PEs.

2. Knowing the new router configuration

  Once a PE has received a teardown on a particular color, it must know
  how to reconfigure its router for the new requested network. This is achieved
  through blocking the teardown task ID until the PE has had the chance to write
  the new requested network configuration to global state.

  As above, because *every* PE in the row or column must participate in *every*
  collective, we know that a PE will eventually hit a library call that
  writes the requested new state to global memory. We just need to make sure
  that this write happens *before* we unblock the teardown task ID.

3. Using global state

  This library depends heavily on reading and writing values to and from
  global state. This may seem like an anti-pattern, and is therefore worthy
  of some explanation.

  Much of the logic in this library is asynchronous and must happen across
  multiple tasks. For instance, calling a primitive is a function call that
  takes place inside the user's task whereas router reconfiguration must happen
  inside the teardown task. Tasks can only accept arguments when they are
  wavelet-triggered, but this mechanism does not fit here: PEs constantly
  sending wavelets to themselves is inefficient, and also, we need more state
  than would fit in a single 32-bit wavelet. Therefore, opt to pass
  state between tasks through globals.

4. Data transfer as a state machine

  The data transfer function looks a lot more like a hardware state machine than
  an ordinary piece of code. This is because some primitives,
  such as ``gather``, require multiple asynchronous operations. To avoid
  using multiple colors to chain these asynchronous operations, we can combine
  them into a single task structured as a state machine.

  This pattern is also one of the reasons that we are required to rely heavily
  on global state.

5. Proxy teardown tasks/colors

  This library is designed to work independently on both the X and Y
  dimensions, supporting both rows and columns of PEs. Mostly, by using a
  different set of colors, a PE's participation in collective communication
  primitives along its row is independent from its participation in collective
  communication primitives along its column.

  Thus, the library may need to teardown colors along both its column and its
  row. Because of this, we can't just define a single teardown task and bind it
  to the teardown task ID (T29 on EIN/FYN). We need separate teardown behavior
  dependent on the color and dimension.

  Instead, we use the virtual teardown mechanism in CSL. Each of the two data
  colors in the X or Y per-PE modules are associated with functions that
  update the global state of the pending variable, and then activates a
  *proxy* teardown task ID. This ID is bound to a task specific to that
  dimension, handling common teardown logic for that dimension.

  Inside the proxy teardown task for a particular dimension, we are able to
  proceed with the teardown as if we were executing within T29.

Reduce Implementation
---------------------

``reduce_fadds`` is implemented quite differently from the other collective
communication primitives. This is because in order to obtain good performance
on larger vectors,
we need to parallelize the reduction across all PEs in the row or column. As
a result, ``reduce_fadds`` uses *2* colors, unlike other primitives which use
just one.

Reconfiguring *into* reduce
^^^^^^^^^^^^^^^^^^^^^^^^^^^

This makes reconfiguring the network into and out of the ``reduce_fadds``
configuration more complicated than the other reconfigurations. To help
illustrate this, here is a step by step example of configuring our network
for ``reduce_fadds`` rooted at PE2. Suppose that we are starting from a
``broadcast`` network with a root of PE3:

.. code-block::

          ┌─────┬─────┬─────┬─────┬─────┐
  color 0 │     │     │     │     │     │
          ▼     ▼     ▼     │     ▼     ▼
        ┌───┐ ┌───┐ ┌───┐ ┌─┴─┐ ┌───┐ ┌───┐
        │PE0│ │PE1│ │PE2│ │PE3│ │PE4│ │PE5│
        └┬──┘ └┬──┘ └┬──┘ └┬──┘ └┬──┘ └┬──┘
  color 1│ ▲   │ ▲   │ ▲   │ ▲   │ ▲   │ ▲
         └─┘   └─┘   └─┘   └─┘   └─┘   └─┘

``broadcast`` only uses color 0 to carry data as it does not need color 1.
Each PE keeps color 1 configured as a ``RAMP -> RAMP`` route to facilitate
easy reconfiguration when color 1 is needed for ``reduce``. To start a
``reduce`` operation, PE3 will send a teardown to all PEs:

.. code-block::

          ┌─────┬─────┬─────┬─────┬─────┐
  color 0 │     │     │     │     │     │
          ▼TD   ▼TD   ▼TD   │TD   ▼TD   ▼TD
        ┌───┐ ┌───┐ ┌───┐ ┌─┴─┐ ┌───┐ ┌───┐
        │PE0│ │PE1│ │PE2│ │PE3│ │PE4│ │PE5│
        └┬──┘ └┬──┘ └┬──┘ └┬──┘ └┬──┘ └┬──┘
  color 1│ ▲   │ ▲   │ ▲   │ ▲   │ ▲   │ ▲
         └─┘   └─┘   └─┘   └─┘   └─┘   └─┘

Inside the teardown task for color 0, we reconfigure color 0 but *also* issue
a new teardown on color 1:

.. code-block::

          ┌─────┐     ┌─────┐     ┌─────┐
  color 0 │     │     │     │     │     │
          │     ▼     ▼     │     ▼     │
        ┌─┴─┐ ┌───┐ ┌───┐ ┌─┴─┐ ┌───┐ ┌─┴─┐
        │PE0│ │PE1│ │PE2│ │PE3│ │PE4│ │PE5│
        └┬──┘ └┬──┘ └┬──┘ └┬──┘ └┬──┘ └┬──┘
  color 1│ ▲TD │ ▲TD │ ▲TD │ ▲TD │ ▲TD │ ▲TD
         └─┘   └─┘   └─┘   └─┘   └─┘   └─┘

The teardown task for color 1 completes the reconfiguration:

.. code-block::

          ┌─────┐     ┌─────┐     ┌─────┐
  color 0 │     │     │     │     │     │
          │     ▼     ▼     │     ▼     │
        ┌─┴─┐ ┌───┐ ┌───┐ ┌─┴─┐ ┌───┐ ┌─┴─┐
        │PE0│ │PE1│ │PE2│ │PE3│ │PE4│ │PE5│
        └┬──┘ └─┬─┘ └───┘ └───┘ └─┬─┘ └┬──┘
  color 1│ ▲    │     ▲     ▲     │    │ ▲
         │ │    │     │     │     │    │ │
         └─┘    └─────┘     └─────┘    └─┘

And we are now able to perform the reduce using ``fadds`` DSD operations.

Reconfiguring *out of* reduce
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Suppose now that the ``reduce`` operation is finished and the user now wants
to ``broadcast`` the result of the reduction (so, PE3 is the root again).

Tearing down this network is not trivial, and we implement it using
a chain of teardowns. First, the PEs at either end send themselves teardown
wavelets on their ``RAMP->RAMP`` colors. This step is unnecessary but makes the
code a bit simpler and is not the main performance drag here.

.. code-block::

          ┌─────┐     ┌─────┐     ┌─────┐
  color 0 │     │     │     │     │     │
          │     ▼     ▼     │     ▼     │
        ┌─┴─┐ ┌───┐ ┌───┐ ┌─┴─┐ ┌───┐ ┌─┴─┐
        │PE0│ │PE1│ │PE2│ │PE3│ │PE4│ │PE5│
        └┬──┘ └─┬─┘ └───┘ └───┘ └─┬─┘ └┬──┘
  color 1│ ▲TD  │     ▲     ▲     │    │ ▲TD
         │ │    │     │     │     │    │ │
         └─┘    └─────┘     └─────┘    └─┘

Inside the teardown task, the PEs reconfigure the color being torn down
as needed and send a new teardown on the opposite color:

.. code-block::

          ┌─────┐     ┌─────┐     ┌─────┐
  color 0 │     │     │     │     │     │
          │TD   ▼TD   ▼     │   TD▼     │TD
        ┌─┴─┐ ┌───┐ ┌───┐ ┌─┴─┐ ┌───┐ ┌─┴─┐
        │PE0│ │PE1│ │PE2│ │PE3│ │PE4│ │PE5│
        └┬──┘ └─┬─┘ └───┘ └───┘ └─┬─┘ └┬──┘
  color 1│ ▲    │     ▲     ▲     │    │ ▲
         │ │    │     │     │     │    │ │
         └─┘    └─────┘     └─────┘    └─┘

Again, inside the teardown task, the PEs reconfigure the color being torn down
as needed and send a new teardown on the opposite color:

.. code-block::

          ┌─────┬─    ┌─────┐    ─┬─────┐
  color 0 │     │     │     │     │     │
          ▼     ▼     ▼     │     ▼     ▼
        ┌───┐ ┌───┐ ┌───┐ ┌─┴─┐ ┌───┐ ┌───┐
        │PE0│ │PE1│ │PE2│ │PE3│ │PE4│ │PE5│
        └┬──┘ └─┬─┘ └───┘ └───┘ └─┬─┘ └┬──┘
  color 1│ ▲  TD│   TD▲     ▲TD   │TD  │ ▲
         │ │    │     │     │     │    │ │
         └─┘    └─────┘     └─────┘    └─┘

And again:

.. code-block::

          ┌─────┬─    ┌─────┐    ─┬─────┐
  color 0 │     │     │TD   │TD   │     │
          ▼     ▼     ▼     │     ▼     ▼
        ┌───┐ ┌───┐ ┌───┐ ┌─┴─┐ ┌───┐ ┌───┐
        │PE0│ │PE1│ │PE2│ │PE3│ │PE4│ │PE5│
        └┬──┘ └┬──┘ └───┘ └───┘ └┬──┘ └┬──┘
  color 1│ ▲   │ ▲               │ ▲   │ ▲
         │ │   │ │               │ │   │ │
         └─┘   └─┘               └─┘   └─┘

Finally giving us:

.. code-block::

          ┌─────┬─────┬─────┬─────┬─────┐
  color 0 │     │     │     │     │     │
          ▼     ▼     ▼     │     ▼     ▼
        ┌───┐ ┌───┐ ┌───┐ ┌─┴─┐ ┌───┐ ┌───┐
        │PE0│ │PE1│ │PE2│ │PE3│ │PE4│ │PE5│
        └┬──┘ └┬──┘ └┬──┘ └┬──┘ └┬──┘ └┬──┘
  color 1│ ▲   │ ▲   │ ▲   │ ▲   │ ▲   │ ▲
         │ │   │ │   │ │   │ │   │ │   │ │
         └─┘   └─┘   └─┘   └─┘   └─┘   └─┘

Which allows us to proceed with the ``broadcast`` rooted at PE3. As discussed
in the Future Work section, there are ways to make this process more efficient,
but they are not yet implemented.

Future Work
-----------

* More efficient reconfiguration out of ``reduce``. All PEs can simultaneously
  send teardowns on their outgoing color.
* If the user calls the same primitive with the same root twice in a row,
  skip reconfiguration and reuse the network.
* Add separate 1 color ``reduce_fadds`` implementation. This will be more
  efficient for short data vectors and would be useful for applications such
  as computing a dot product.
* Allow FIFOs as inputs to collective communication primitives.
