2D Collective Communication Library
===================================

get_params
----------

Compute the parameters necessary to import ``<collectives_2d>`` on a given PE.

Declaration
^^^^^^^^^^^

.. code-block:: csl

  fn get_params(Px: u16, Py: u16, ids: comptime_struct) -> comptime_struct

Where:

* ``Px`` is the PE's x-coordinate.
* ``Py`` is the PE's y-coordinate.
* ``ids`` is a struct that is expected to have either the ``x``-related fields,
  the ``y``-related fields, or all four, of the following:

  * ``x_colors``: a struct containing 2 distinct colors as anonymous fields
  * ``x_entrypoints``: a struct containing 2 distinct local task IDs as
    anonymous fields
  * ``y_colors``: a struct containing 2 distinct colors as anonymous fields
  * ``y_entrypoints``: a struct containing 2 distinct local task IDs as
    anonymous fields

* Returns a struct containing the parameters necessary to import
  library modules for the specified PE. This struct contains:

  * ``x``: an opaque struct containing parameters needed to configure
    collective communications in the x-dimension.
  * ``y``: an opaque struct containing parameters needed to configure
    collective communications in the y-dimension.

Example
^^^^^^^

.. code-block:: csl

  // Get params
  const p = get_params(0, 0, .{
    .x_colors      = .{ @get_color(0),         @get_color(1) },
    .x_entrypoints = .{ @get_local_task_id(2), @get_local_task_id(3) },
    .y_colors      = .{ @get_color(4),         @get_color(5) },
    .y_entrypoints = .{ @get_local_task_id(6), @get_local_task_id(7) },
  });

  // Import library modules using these params
  const c2d_x = @import_module("<collectives_2d/pe>", .{ .dim_params = p.x });
  const c2d_y = @import_module("<collectives_2d/pe>", .{ .dim_params = p.y });

Semantics
^^^^^^^^^

The ``ids`` struct may contain fields ``x_colors``, ``x_entrypoints``,
``y_colors``, and ``y_entrypoints``.
If ``x_colors`` and ``x_entrypoints`` are included, for instance,
these fields should specify a set of 2 unique colors and 2 unique local task IDs
that the library will use when implementing collective communications in the
``x`` dimension.

Colors and task IDs may not be shared across dimensions.

If the ``ids`` struct only contains the pair of fields for one dimension,
then ``<collectives_2d/pe>`` may only be imported for that dimension.

All colors passed to ``get_params`` are considered to be "owned" by the library
and should not be activated or used in DSD operations outside of library calls.

init
----

Initialize the library at runtime.

Declaration
^^^^^^^^^^^

.. code-block:: csl

  fn init() -> void

Example
^^^^^^^

.. code-block:: csl

  const c2d_x = @import_module("<collectives_2d/pe>", .{ .dim_params = p.x });

  task main() void {
    c2d_x.init();
    ...
  }

Semantics
^^^^^^^^^

``init()`` must be called for each ``<collectives_2d/pe>`` module imported
before using any collective communication primitives from that module. In
effect, this means calling it once per dimension at the start of program
execution.

broadcast
---------

Send a data array from a root PE to all other PEs in the row or column.

Declaration
^^^^^^^^^^^

.. code-block:: csl

  fn broadcast(root: u16, buf: [*]u32, count: u16, callback: color) -> void

Where:

* ``root`` is the index of the root PE within the row or column.
* At the root PE, ``buf`` is the data to be sent to other PEs, at the
  non-root PEs, ``buf`` is the destination buffer the data will be received
  into.
* ``count`` is the length of ``buf``.
* ``callback`` is a color that is activated once ``broadcast`` is complete.

Example
^^^^^^^

.. code-block::

  PE0 buf = [ 10 11 12 13 14 15 16 17 ]
      broadcast(1, &buf, 8, callback)
      buf = [ 18 19 20 21 22 23 24 25 ]

  PE1 buf = [ 18 19 20 21 22 23 24 25 ]
      broadcast(1, &buf, 8, callback)
      buf = [ 18 19 20 21 22 23 24 25 ]

  PE2 buf = [ 26 27 28 29 30 31 32 33 ]
      broadcast(1, &buf, 8, callback)
      buf = [ 18 19 20 21 22 23 24 25 ]

  PE3 buf = [ 34 35 36 37 38 39 40 41 ]
      broadcast(1, &buf, 8, callback)
      buf = [ 18 19 20 21 22 23 24 25 ]

Semantics
^^^^^^^^^

All PEs in the row or column must call ``broadcast`` with the same value of
``root`` and ``count``.

``broadcast`` is an asynchronous operation. At the ``root`` PE, ``callback``
will be activated when sending ``count`` many elements from ``buf`` is
complete.
At non-``root`` PEs,
``callback`` is activated when ``count``-many elements have been received into
``buf``.

scatter
-------

Distribute equal sized chunks of an array form a root PE to all PEs in the
row or column.

Declaration
^^^^^^^^^^^

.. code-block:: csl

  fn scatter(root: u16, send_buf: [*]u32, recv_buf: [*]u32,
             count: u16, callback: color) -> void

Where:

* ``root`` is the index of the root PE within the row or column.
* ``send_buf`` is the data to be distributed between all PEs.
* ``recv_buf`` is the buffer into which data will be received.
* ``count`` is the size of the data chunk that each PE will receive.
* ``callback`` is a color that is activated once ``scatter`` is complete.

Example
^^^^^^^

.. code-block::

  PE0 send_buf = [ ] recv_buf = [ 0 0 ]
      scatter(1, &send_buf, &recv_buf, 2, callback)
      send_buf = [ ] recv_buf = [ 0 1 ]

  PE1 send_buf = [ 0 1 2 3 4 5 6 7 ] recv_buf = [ 0 0 ]
      scatter(1, &send_buf, &recv_buf, 2, callback)
      send_buf = [ 0 1 2 3 4 5 6 7 ] recv_buf = [ 2 3 ]

  PE2 send_buf = [ ] recv_buf = [ 0 0 ]
      scatter(1, &send_buf, &recv_buf, 2, callback)
      send_buf = [ ] recv_buf = [ 4 5 ]

  PE3 send_buf = [ ] recv_buf = [ 0 0 ]
      scatter(1, &send_buf, &recv_buf, 2, callback)
      send_buf = [ ] recv_buf = [ 6 7 ]


Semantics
^^^^^^^^^

All PEs in the row or column must call ``scatter`` with the same value of
``root`` and ``count``.

The ``root`` PE will send ``count``-many elements to *each* PE in the row or
column and requires ``send_buf``
to be a ``count * NUM_PES`` sized array, where ``NUM_PES`` is the number of
PEs in the row or column.

Each PE, including the root itself will receive ``count``-many elements into
its ``recv_buf``. Data is distributed in array order: PE0 will always receive
the first ``count`` elements, PE1 will receive the next ``count`` elements,
etc.

``scatter`` is an asynchronous operation. At the ``root`` PE, ``callback``
will be activated when sending all of ``send_buf`` is complete.
At non-``root`` PEs,
``callback`` is activated when ``count``-many elements have been received into
``recv_buf``.

``scatter`` is the inverse of ``gather``.

gather
------

Each PE sends equal sized chunks of data to a root PE. The root PE appends each
received chunk into a larger array.

Declaration
^^^^^^^^^^^

.. code-block:: csl

  fn gather(root: u16, send_buf: [*]u32, recv_buf: [*]u32,
            count: u16, callback: color) -> void

Where:

* ``root`` is the index of the root PE within the row or column.
* ``send_buf`` is the data to be sent to the root.
* ``recv_buf`` is the buffer into which data will be received at the root.
* ``count`` is the size of the data chunk that each PE will send.
* ``callback`` is a color that is activated once ``gather`` is complete.

Example
^^^^^^^

.. code-block::

  PE0 send_buf = [ 0 1 ] recv_buf = [ ]
      gather(1, &send_buf, &recv_buf, 2, callback)
      send_buf = [ 0 1 ] recv_buf = [ ]

  PE1 send_buf = [ 2 3 ] recv_buf = [ 0 0 0 0 0 0 0 0 ]
      gather(1, &send_buf, &recv_buf, 2, callback)
      send_buf = [ 2 3 ] recv_buf = [ 0 1 2 3 4 5 6 7 ]

  PE2 send_buf = [ 4 5 ] recv_buf = [ ]
      gather(1, &send_buf, &recv_buf, 2, callback)
      send_buf = [ 4 5 ] recv_buf = [ ]

  PE3 send_buf = [ 6 7 ] recv_buf = [ ]
      gather(1, &send_buf, &recv_buf, 2, callback)
      send_buf = [ 6 7 ] recv_buf = [ ]

Semantics
^^^^^^^^^

All PEs in the row or column must call ``gather`` with the same value of
``root`` and ``count``.

All PEs, including the ``root``, will send ``count``-many elements from their
``send_buf`` s.

The root PE will receive ``count * NUM_PES`` many elements into its
``recv_buf``, where ``NUM_PES`` is the number of PEs in the row or column.
``recv_buf`` must be of size ``count * NUM_PES`` on the ``root`` PE.
``recv_buf`` will be populated with elements in PE order. The first ``count``
elements in ``recv_buf`` will be from PE0, the next ``count`` elements
will be from PE1, etc.

``gather`` is an asynchronous operation. At the ``root`` PE, ``callback``
will be activated when receiving all of ``recv_buf`` is complete.
At non-``root`` PEs,
``callback`` is activated when ``count``-many elements have been sent
from ``send_buf``.

``gather`` is the inverse of ``scatter``.

reduce_fadds
------------

Reduce data vectors from each PE into a single vector at the root PE using
``fadds`` as a reduction operator.

Declaration
^^^^^^^^^^^

.. code-block:: csl

  fn reduce_fadds(root: u16, send_buf: [*]f32, recv_buf: [*]f32,
                  count: u16, callback: color) -> void

Where:

* ``root`` is the index of the root PE within the row or column.
* ``send_buf`` is the data to be sent to the root.
* ``recv_buf`` is the buffer to store the reduced vector at the root.
* ``count`` is the size of the data chunk that each PE will contribute to the
  result.
* ``callback`` is a color that is activated once ``reduce_fadds`` is complete.

Example
^^^^^^^

.. code-block::

  PE0 send_buf = [ 10. 11. 12. 13. ] recv_buf = [ ]
      reduce_fadds(1, &send_buf, &recv_buf, 4, callback)
      send_buf = [ 10. 11. 12. 13. ] recv_buf = [ ]

  PE1 send_buf = [ 14. 15. 16. 17. ] recv_buf = [ 0. 0. 0. 0. ]
      reduce_fadds(1, &send_buf, &recv_buf, 4, callback)
      send_buf = [ 14. 15. 16. 17. ] recv_buf = [ 64. 68. 72. 76. ]

  PE2 send_buf = [ 18. 19. 20. 21. ] recv_buf = [ ]
      reduce_fadds(1, &send_buf, &recv_buf, 4, callback)
      send_buf = [ 18. 19. 20. 21. ] recv_buf = [ ]

  PE3 send_buf = [ 22. 23. 24. 25. ] recv_buf = [ ]
      reduce_fadds(1, &send_buf, &recv_buf, 4, callback)
      send_buf = [ 22. 23. 24. 25. ] recv_buf = [ ]

Semantics
^^^^^^^^^

All PEs in the row or column must call ``reduce_fadds`` with the same value of
``root`` and ``count``.

Each PE ``p``, including the ``root``, will contribute a vector ``v_p`` of
size ``count``
to the final result. The final result will be ``sum(v_p : 0 <= p < NUM_PES)``
where
``NUM_PES`` is the number of PEs in the row or column. This final result will
only be stored in the ``root`` PE's ``recv_buf``.

``reduce_fadds`` is an asynchronous operation. At the ``root`` PE, ``callback``
will be activated when the full result is stored in ``recv_buff``.
At non-``root`` PEs,
``callback`` is activated when ``count``-many elements have been contributed
from ``send_buf``.
