#!/usr/bin/env bash

# set -e
sqrt_result=$(echo "sqrt(512)" | bc -l)
int_result=${sqrt_result%.*}

# Run this to check whether everything works and calibrate the sync
# cslc layout_1d_test.csl --fabric-dims=15,3 \
# --fabric-offsets=4,1 --params=Nx_start:256,Pw:8,Ph:1,Algo:3,is_allred:0,step:$int_result -o out --memcpy --channels=1
# cs_python run_2d_test.py --name out

  cslc layout_1d_test.csl --fabric-dims=135,3 \
--fabric-offsets=4,1 --params=Nx_start:256,Pw:128,Ph:1,Algo:3,is_allred:0,step:$int_result,tmp:2 -o out --memcpy --channels=1
cs_python run_2d_test.py --name out

#   cslc layout_1d_test.csl --fabric-dims=135,3 \
# --fabric-offsets=4,1 --params=Nx_start:256,Pw:128,Ph:1,Algo:3,is_allred:0,step:$int_result,tmp:2 -o out --memcpy --channels=1
# cs_python run_2d_test.py --name out

#   cslc layout_1d_test.csl --fabric-dims=135,3 \
# --fabric-offsets=4,1 --params=Nx_start:256,Pw:128,Ph:1,Algo:3,is_allred:0,step:$int_result,tmp:3 -o out --memcpy --channels=1
# cs_python run_2d_test.py --name out

# for algo in {3..3}
# do
#   #cslc layout.csl --fabric-dims=519,3 \
#   cslc layout_1d_test.csl --fabric-dims=15,3 \
#   --fabric-offsets=4,1 --params=Nx_start:1,Pw:8,Ph:1,Algo:$algo,is_allred:0,step:$int_result -o out --memcpy --channels=1
#   cs_python run_2d_test.py --name out
#   # cs_python run_1d.py --name out --cmaddr $CS_IP_ADDR
# done


# for algo in {0..3}
# do
#   sqrt_result=$(echo "sqrt(512)" | bc -l)
#   int_result=${sqrt_result%.*}
#   #cslc layout.csl --fabric-dims=519,3 \
#   cslc layout_1d_test.csl --fabric-dims=512,3 \
#   --fabric-offsets=4,1 --params=Nx_start:1,Pw:512,Ph:1,Algo:$algo,is_allred:1,step:$int_result -o out --memcpy --channels=1
#   cs_python run_2d_test.py --name out
#   # cs_python run_1d.py --name out --cmaddr $CS_IP_ADDR
# done
