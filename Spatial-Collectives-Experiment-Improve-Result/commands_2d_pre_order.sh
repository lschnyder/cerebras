#!/usr/bin/env bash

# set -e

# Benchmarks for fixed number of PEs and increasing vector length
#reduce
for algo in {0..0}
do
  for vec_len in {0..13}
  do
    for repeat in {1..1}
    do
      python generate_pre_order_2d.py 512 $((2**$vec_len)) "x"
      sqrt_result=$(echo "sqrt(512)" | bc -l)
      int_result=${sqrt_result%.*}
      #cslc layout_2d.csl --fabric-dims=519,514 \
      cslc layout_2d_pre_order.csl --fabric-dims=757,996 \
      --fabric-offsets=4,1 --params=Nx:$((2**$vec_len)),Pw:512,Ph:512,Algo:$algo,is_allred:0,step:$int_result -o out --memcpy --channels=1
      #cs_python run_2d.py --name out
      cs_python run_2d.py --name out --cmaddr $CS_IP_ADDR
    done
  done
done

for algo in {0..0}
do
  for vec_len in {0..13}
  do
    for repeat in {1..1}
    do
      python generate_pre_order_2d.py 512 $((2**$vec_len)) "x"
      sqrt_result=$(echo "sqrt(512)" | bc -l)
      int_result=${sqrt_result%.*}
      #cslc layout_2d.csl --fabric-dims=519,514 \
      cslc layout_2d_pre_order.csl --fabric-dims=757,996 \
      --fabric-offsets=4,1 --params=Nx:$((2**$vec_len)),Pw:512,Ph:512,Algo:$algo,is_allred:1,step:$int_result -o out --memcpy --channels=1
      #cs_python run_2d.py --name out
      cs_python run_2d.py --name out --cmaddr $CS_IP_ADDR
    done
  done
done

# Benchmarks for fixed vector length and increasing number of PEs
Nx=256
#reduce
for algo in {0..0}
do
  for log_pes in {2..9}
  do
    for repeat in {1..1}
    do
      python generate_pre_order_2d.py $((2**$log_pes)) 256 "x"
      sqrt_result=$(echo "sqrt(2^$log_pes)" | bc -l)
      int_result=${sqrt_result%.*}
      #cslc layout_2d.csl --fabric-dims=$((2**$log_pes + 7)),$((2**$log_pes + 2)) \
      cslc layout_2d_pre_order.csl --fabric-dims=757,996 \
      --fabric-offsets=4,1 --params=Nx:256,Pw:$((2**$log_pes)),Ph:$((2**$log_pes)),Algo:$algo,is_allred:0,step:$int_result -o out --memcpy --channels=1
      #cs_python run_2d.py --name out
      cs_python run_2d.py --name out --cmaddr $CS_IP_ADDR
    done
  done
done

for algo in {0..0}
do
  for log_pes in {2..9}
  do
    for repeat in {1..1}
    do
      python generate_pre_order_2d.py $((2**$log_pes)) 256 "x"
      sqrt_result=$(echo "sqrt(2^$log_pes)" | bc -l)
      int_result=${sqrt_result%.*}
      #cslc layout_2d.csl --fabric-dims=$((2**$log_pes + 7)),$((2**$log_pes + 2)) \
      cslc layout_2d_pre_order.csl --fabric-dims=757,996 \
      --fabric-offsets=4,1 --params=Nx:256,Pw:$((2**$log_pes)),Ph:$((2**$log_pes)),Algo:$algo,is_allred:1,step:$int_result -o out --memcpy --channels=1
      #cs_python run_2d.py --name out
      cs_python run_2d.py --name out --cmaddr $CS_IP_ADDR
    done
  done
done
