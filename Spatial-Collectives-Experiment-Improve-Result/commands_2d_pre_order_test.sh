#!/usr/bin/env bash

# set -e

# Run this to check whether everything works and calibrate the sync
for algo in {0..0}
do
  for vec_len in {0..12}
    do
    python generate_pre_order_2d.py 512 $((2**$vec_len)) "x"
    sqrt_result=$(echo "sqrt(512)" | bc -l)
    int_result=${sqrt_result%.*}
    #cslc layout.csl --fabric-dims=519,3 \
    cslc layout_2d_pre_order_test.csl --fabric-dims=519,3 \
    --fabric-offsets=4,1 --params=Nx_start:$((2**$vec_len)),Pw:512,Ph:512,Algo:$algo,is_allred:0,step:$int_result -o out --memcpy --channels=1
    cs_python run_2d_specific_pe_test.py --name out
    # cs_python run_1d.py --name out --cmaddr $CS_IP_ADDR
  done
done

for algo in {0..0}
do
  for log_pes in {2..8}
  do
    for repeat in {1..1}
    do
      python generate_pre_order_2d.py $((2**$log_pes)) 256 "x"
      sqrt_result=$(echo "sqrt(2^$log_pes)" | bc -l)
      int_result=${sqrt_result%.*}
      #cslc layout_2d.csl --fabric-dims=$((2**$log_pes + 7)),$((2**$log_pes + 2)) \
      cslc layout_2d_pre_order_test.csl --fabric-dims=519,3 \
      --fabric-offsets=4,1 --params=Nx_start:256,Pw:$((2**$log_pes)),Ph:$((2**$log_pes)),Algo:$algo,is_allred:0,step:$int_result -o out --memcpy --channels=1
      cs_python run_2d_specific_pe_test.py --name out
    done
  done
done