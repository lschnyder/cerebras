#!/usr/bin/env bash

# set -e

# Benchmarks for fixed number of PEs and increasing vector length
#reduce

for vec_len in {0..12}
do
  rm -rf pre_order
  mkdir pre_order
  python generate_pre_order.py 512 $((2**vec_len))
  for repeat in {1..5}
  do
    #cslc layout_pre_order.csl --fabric-dims=39,3 \
    cslc layout_pre_order.csl --fabric-dims=757,996 \
    --fabric-offsets=4,1 --params=Nx:$((2**$vec_len)),Pw:512,Algo:0,is_allred:0,step:0 -o out --memcpy --channels=1
    #cs_python run_pre_order.py --name out
    cs_python run_pre_order.py --name out --cmaddr $CS_IP_ADDR
  done
  rm layout_pre_order.csl
done

# allreduce
for vec_len in {0..12}
do
  rm -rf pre_order
  mkdir pre_order
  python generate_pre_order.py 512 $((2**vec_len))
  for repeat in {1..5}
  do
    #cslc layout_pre_order.csl --fabric-dims=519,3 \
    cslc layout_pre_order.csl --fabric-dims=757,996 \
    --fabric-offsets=4,1 --params=Nx:$((2**$vec_len)),Pw:512,Algo:0,is_allred:1,step:0 -o out --memcpy --channels=1
    #cs_python run_pre_order.py --name out
    cs_python run_pre_order.py --name out --cmaddr $CS_IP_ADDR
  done
  rm layout_pre_order.csl
done

# Benchmarks for fixed vector length and increasing number of PEs
Nx=256
#reduce
for log_pes in {2..9}
do
  for repeat in {1..5}
  do
    rm -rf pre_order
    mkdir pre_order
    python generate_pre_order.py $((2**$log_pes + 7)) 256
    #cslc layout_pre_order.csl --fabric-dims=$((2**$log_pes + 7)),3 \
    cslc layout_pre_order.csl --fabric-dims=757,996 \
    --fabric-offsets=4,1 --params=Nx:256,Pw:$((2**$log_pes)),Algo:0,is_allred:0,step:0 -o out --memcpy --channels=1
    #cs_python run_pre_order.py --name out
    cs_python run_pre_order.py --name out --cmaddr $CS_IP_ADDR
    rm layout_pre_order.csl
  done
done

#allreduce
for log_pes in {2..9}
do
  for repeat in {1..5}
  do
    rm -rf pre_order
    mkdir pre_order
    python generate_pre_order.py $((2**$log_pes + 7)) 256
    #cslc layout_pre_order.csl --fabric-dims=$((2**$log_pes + 7)),3 \
    cslc layout_pre_order.csl --fabric-dims=757,996 \
    --fabric-offsets=4,1 --params=Nx:256,Pw:$((2**$log_pes)),Algo:0,is_allred:1,step:0 -o out --memcpy --channels=1
    #cs_python run_pre_order.py --name out
    cs_python run_pre_order.py --name out --cmaddr $CS_IP_ADDR
    rm layout_pre_order.csl
  done
done
