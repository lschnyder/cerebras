#!/usr/bin/env bash

# set -e

# Run this to check whether everything works and calibrate the sync
for algo in {0..1}
do
  for vec_len in {0..12}
  do
    for repeat in {1..1}
    do
      sqrt_result=$(echo "sqrt(512)" | bc -l)
      int_result=${sqrt_result%.*}
      cslc layout_test_chain.csl --fabric-dims=757,996 \
      --fabric-offsets=4,1 --params=Nx:$((2**$vec_len)),Pw:512,Algo:$algo,is_allred:1,step:$int_result -o out --memcpy --channels=1
      cs_python run_test_chain.py --name out --cmaddr $CS_IP_ADDR
    done
  done
done

for algo in {0..1}
do
  for log_pes in {2..9}
  do
    #for repeat in {1..5}
    #do
      sqrt_result=$(echo "sqrt(2^$log_pes)" | bc -l)
      int_result=${sqrt_result%.*}
      #cslc layout.csl --fabric-dims=$((2**$log_pes + 7)),3 \
      cslc layout_test_chain.csl --fabric-dims=757,996 \
      --fabric-offsets=4,1 --params=Nx:256,Pw:$((2**$log_pes)),Algo:$algo,is_allred:0,step:$int_result -o out --memcpy --channels=1
      #cs_python run_1d.py --name out
      cs_python run_test_chain.py --name out --cmaddr $CS_IP_ADDR
    #done
  done
done