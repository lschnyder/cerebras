#!/usr/bin/env bash

# set -e

# Run this to check whether everything works and calibrate the sync
for algo in {0..3}
do
  for vec_len in {0..12}
  do
    for repeat in {1..1}
    do
      sqrt_result=$(echo "sqrt(512)" | bc -l)
      int_result=${sqrt_result%.*}
      #cslc layout_2d.csl --fabric-dims=519,514 \
      cslc layout_2d_pre_order.csl --fabric-dims=519,514 \
      --fabric-offsets=4,1 --params=Nx:$((2**$vec_len)),Pw:512,Ph:512,Algo:$algo,is_allred:0,step:$int_result -o out --memcpy --channels=1
      cs_python run_2d.py --name out
      # cs_python run_2d.py --name out --cmaddr $CS_IP_ADDR
    done
  done
done
