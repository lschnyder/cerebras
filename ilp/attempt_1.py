import itertools
from pulp import LpMinimize, LpProblem, LpStatus, lpSum, LpVariable, PULP_CBC_CMD

# Define problem dimensions
M = 3
N = 1
B = 1

# TODO: Replace router by inbound and outbound routers

#Upper Bound on the sending cost
upper_bound = 1000

# Create the problem
model = LpProblem(name="optimal-reduce", sense=LpMinimize)

# Define variables
e_offramp = {(x,y,b): LpVariable(name=f"e_offramp_{x}_{y}_{b}", cat="Integer", lowBound=0) for x in range(M) for y in range(N) for b in range(B)}
e_onramp = {(x,y,b): LpVariable(name=f"e_onramp_{x}_{y}_{b}", cat="Binary") for x in range(M) for y in range(N) for b in range(B)}

reaches_root = {(x,y,b): LpVariable(name=f"reaches_root_{x}_{y}_{b}", cat="Binary") for x in range(M) for y in range(N) for b in range(B)}

# Define grid variables
e_grid = {(x,y,x+1,y,b): LpVariable(name=f"start_{x}_{y}_end_{x+1}_{y}_index_{b}", cat="Integer", lowBound=0) for x in range(M-1) for y in range(N) for b in range(B)}
e_grid |= {(x,y,x-1,y,b):LpVariable(name=f"start_{x}_{y}_end_{x-1}_{y}_index_{b}", cat="Integer", lowBound=0) for x in range(1, M) for y in range(N) for b in range(B)}
e_grid |= {(x,y,x,y+1,b):LpVariable(name=f"start_{x}_{y}_end_{x}_{y+1}_index_{b}", cat="Integer", lowBound=0) for x in range(M) for y in range(N-1) for b in range(B)}
e_grid |= {(x,y,x,y-1,b):LpVariable(name=f"start_{x}_{y}_end_{x}_{y-1}_index_{b}", cat="Integer", lowBound=0) for x in range(M) for y in range(1, N) for b in range(B)}

print({k: v for k,v in e_grid.items() if k[0]==0})

# Flow Conservation Constraints
for x in range(M):
    for y in range(N):
        for b in range(B):
            # grid_out_flow = lpSum([e for e in e_grid if f"start_{x}_{y}" in e.name and f"index_{b}" in e.name])
            grid_out_flow = lpSum([v for k,v in e_grid.items() if k[0]==x and k[1]==y and k[4]==b])
            # ramp_out_flow = [e for e in e_offramp if f"e_offramp_{x}_{y}_{b}" in e.name]
            ramp_out_flow = [v for k,v in e_offramp.items() if k==(x,y,b)]
            # grid_in_flow = lpSum([e for e in e_grid if f"end_{x}_{y}" in e.name and f"index_{b}" in e.name])
            grid_in_flow = lpSum([v for k,v in e_grid.items() if k[2]==x and k[3]==y and k[4]==b])
            # ramp_in_flow = [e for e in e_onramp if f"e_onramp_{x}_{y}_{b}" in e.name]
            ramp_in_flow = [v for k,v in e_onramp.items() if k==(x,y,b)]
            
            out_flow = grid_out_flow + ramp_out_flow[0]
            in_flow = grid_in_flow + ramp_in_flow[0]
            model += (out_flow == in_flow, f"flow_conservation_{x}_{y}_{b}")

# Tree constraint
for b in range(B):
    model += (reaches_root[0,0,b] == 1, "root_reaches_itself_{b}")

for x in range(M):
    for y in range(N):
        for b in range(B):
            if x != 0 or y != 0:
                model += (lpSum([v for k,v in e_grid.items() if k[0] == x and k[1] == y and k[2] == b]) >= 1, f"enforce_sending_anywhere_{x}_{y}_{b}")
            for u in range(x-1,x+2):
                for v in range(y-1,y+1):
                    try:
                        e_grid[x,y,u,v,b]
                        model += (reaches_root[x,y,b] <= reaches_root[u,v,b] - e_grid[x,y,u,v,b]*0.000001, f"enforce_reachability_{x}_{y}_{u}_{v}_{b}")
                    except KeyError:
                        continue

# V = [(x,y) for x in range(M) for y in range(N)]
# for S in itertools.chain.from_iterable(itertools.combinations(V, r) for r in range(1, len(V))):
#     S = set(S)
#     delta_S = {k: v for k,v in e_grid.items() if ((k[0],k[1]) in S and (k[2],k[3]) not in S) or ((k[2],k[3]) in S and (k[0],k[1]) not in S)}
#     if delta_S:
#         model += lpSum(e_grid[e] for e in delta_S) >= 1

# model += lpSum(e_grid[e] for e in e_grid) == len(V) - 1

# for x in range(M):
#     for y in range(N):
#         for b in range(B):
#             if (x, y) != (0, 0):
#                 # Each node other than the root must have a path to the root
#                 model += reaches_root[x * N * B + y * B + b] == 1, f"must_reach_root_{x}_{y}_{b}"
#                 # Constraint to ensure that if a node reaches the root, at least one outgoing edge from this node must reach another node or the root
                
#                 grid_outgoing = lpSum([e for e in e_grid if f"start_{x}_{y}" in e.name and f"index_{b}" in e.name])
#                 model += grid_outgoing >= reaches_root[x * N * B + y * B + b], f"outgoing_from_{x}_{y}_{b}"


# Reduce Constraints
for k,v in e_onramp.items():
    if k[0] == k[1] == 0:
        model += (v == 0, f"root_never_sends_{k}")
    else:
        model += (v == 1, f"non_root_sends_once_{k}")

# Objective Function
obj1 = lpSum(e_grid)
obj2 = lpSum([1 - v for k,v in reaches_root.items()])
model += obj1 + upper_bound * obj2


# Solve the model
status = model.solve(PULP_CBC_CMD(msg=1))

# Print the results
print(f"status: {LpStatus[model.status]}")
print(f"objective: {model.objective.value()}")

for var in model.variables():
    print(f"{var.name}: {var.value()}")

for name, constraint in model.constraints.items():
    print(f"{name}: {constraint.value()}")
