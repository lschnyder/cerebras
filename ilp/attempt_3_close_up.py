import itertools
import matplotlib.pyplot as plt
import networkx as nx
from pulp import LpMinimize, LpProblem, LpStatus, lpSum, LpVariable, PULP_CBC_CMD
import numpy as np

def draw_multigraph(G, pos):
    # Draw nodes and labels
    nx.draw_networkx_nodes(G, pos, node_size=700)
    nx.draw_networkx_labels(G, pos, font_size=14, font_family='sans-serif')
    
    # Dictionary to keep track of the count of parallel edges
    parallel_edges_count = {}
    
    # Iterate over all edges
    for u, v, data in G.edges(data=True):
        if (u, v) not in parallel_edges_count:
            parallel_edges_count[(u, v)] = 0
        else:
            parallel_edges_count[(u, v)] += 1
        
        # Compute the offset for this edge
        count = parallel_edges_count[(u, v)]
        offset = np.array([0, 0.1 * (count + 1)])  # Adjust the multiplier for more spacing
        
        # Get the position of the nodes
        pos_u = np.array(pos[u])
        pos_v = np.array(pos[v])
        
        # Compute the displacement vector
        displacement = pos_v - pos_u
        displacement = displacement / np.linalg.norm(displacement)
        perpendicular = np.array([-displacement[1], displacement[0]])
        
        # Offset the edge positions
        pos_u = pos_u + offset[0] * perpendicular
        pos_v = pos_v + offset[1] * perpendicular
        
        # Draw the edge
        plt.plot([pos[u][0], pos_v[0]], [pos[u][1], pos_v[1]], 'k-', alpha=0.5)
        # Draw the arrowhead
        plt.arrow(pos_v[0], pos_v[1], -0.05*displacement[0], -0.05*displacement[1], shape='full', lw=0, length_includes_head=True, head_width=0.05)
        
        # Draw edge labels (weights)
        mid_point = (pos[u] + pos[v]) / 2
        mid_point += offset * 0.5 * perpendicular
        plt.text(mid_point[0], mid_point[1], s=data['weight'], horizontalalignment='center', verticalalignment='center', bbox=dict(facecolor='white', edgecolor='none', alpha=0.6))

# Define problem dimensions
M = 2
N = 2
B = 1

#Upper Bound on the sending cost
large_constant = M*N

# Create the problem
model = LpProblem(name="optimal-reduce", sense=LpMinimize)

# Define variables
e_offramp = {(x,y,b,d): LpVariable(name=f"e_offramp_{x}_{y}_{b}_{d}_{t}", cat="Binary") for x in range(M) for y in range(N) for b in range(B) for d in range(1,N*M+1) for t in range(1,N*M*B+1)}
e_onramp = {(x,y,b,d): LpVariable(name=f"e_onramp_{x}_{y}_{b}_{d}_{t}", cat="Binary") for x in range(M) for y in range(N) for b in range(B) for d in range(1,N*M+1) for t in range(1,N*M*B+1)}
# d is the density of a wavelet, ie how many wavelets have been merged into it.

r = {(x,y,b,d,t): LpVariable(name=f"router{x}_{y}_{b}_{d}_{t}", cat="Integer") for x in range(M) for y in range(N) for b in range(B) for d in range(1,N*M+1) for t in range(1,N*M*B+1)}
c = {(x,y,b,d,t): LpVariable(name=f"processor{x}_{y}_{b}_{d}_{t}", cat="Integer") for x in range(M) for y in range(N) for b in range(B) for d in range(1,N*M+1) for t in range(1,N*M*B+1)}

# Define grid variables
e_grid = {(x,y,x+1,y,b,d,t): LpVariable(name=f"start_{x}_{y}_end_{x+1}_{y}_i{b}_d{d}_t{t}", cat="Binary") for x in range(M-1) for y in range(N) for b in range(B) for d in range(1,N*M+1) for t in range(1,N*M*B+1)}
e_grid |= {(x,y,x-1,y,b,d,t):LpVariable(name=f"start_{x}_{y}_end_{x-1}_{y}_i{b}_d{d}_t{t}", cat="Binary") for x in range(1, M) for y in range(N) for b in range(B) for d in range(1,N*M+1) for t in range(1,N*M*B+1)}
e_grid |= {(x,y,x,y+1,b,d,t):LpVariable(name=f"start_{x}_{y}_end_{x}_{y+1}_i{b}_d{d}_t{t}", cat="Binary") for x in range(M) for y in range(N-1) for b in range(B) for d in range(1,N*M+1) for t in range(1,N*M*B+1)}
e_grid |= {(x,y,x,y-1,b,d,t):LpVariable(name=f"start_{x}_{y}_end_{x}_{y-1}_i{b}_d{d}_t{t}", cat="Binary") for x in range(M) for y in range(1, N) for b in range(B) for d in range(1,N*M+1) for t in range(1,N*M*B+1)}


# Flow Conservation Constraints
for x in range(M):
    for y in range(N):
        for b in range(B):
            for d in range(1,M*N):
                for t in range(1,N*M*B):
                    grid_out_flow_next = lpSum([v for k,v in e_grid.items() if k[0]==x and k[1]==y and k[4]==b and k[5]==d and k[6]==t+1])
                    ramp_out_flow_next = e_offramp[x,y,b,d,t+1]
                    grid_in_flow_next = lpSum([v for k,v in e_grid.items() if k[2]==x and k[3]==y and k[4]==b and k[5]==d and k[6]==t+1])
                    ramp_in_flow_next = e_onramp[x,y,b,d,t+1]
                    grid_in_flow_now = lpSum([v for k,v in e_grid.items() if k[2]==x and k[3]==y and k[4]==b and k[5]==d and k[6]==t])
                    ramp_in_flow_now = e_onramp[x,y,b,d,t]

                    model += (grid_out_flow_next + ramp_out_flow_next == grid_in_flow_now + ramp_in_flow_now, f"flow_conservation_{x}_{y}_{b}_{d}")

for x in range(M):
    for y in range(N):
        for b in range(B):
            total_in_density = lpSum([v*k[3] for k,v in e_offramp.items() if k[0] == x and k[1] == y and k[2] == b])
            if x == 0 and y == 0:
                model += (total_in_density == M*N-1, f"all_paths_lead_to_root_{b}")
                for d in range(1,M*N):
                    model += (e_onramp[x,y,b,d] == 0, f"root_never_sends_{b}_{d}")
            else:
                for in_density in range(0,M*N-1):
                    # model += (e_onramp[x,y,b,in_density] <= in_density - total_in_density, f"non-root sends with one more density")
                    model += (total_in_density - in_density <= large_constant * (1 - e_onramp[x,y,b,in_density+1]), f"non-root_sends_with_one_more_density_first_{x}_{y}_{b}_{in_density+1}")
                    model += (in_density - total_in_density <= large_constant * (1 - e_onramp[x,y,b,in_density+1]), f"non-root_sends_with_one_more_density_second_{x}_{y}_{b}_{in_density+1}")

# Contention
contention_ub = LpVariable(name="contention_ub", cat="Integer", lowBound=0)
for x in range(M):
    for y in range(N):
        c = lpSum([v for k,v in e_offramp.items() if k[0]==x and k[1]==y])
        model += (contention_ub >= c, f"contention_at_{x}_{y}")

# Close-up
for x in range(M):
    for y in range(N):
        for b in range(B):
            for d in range(1,M*N):
                LpVariable("last_active_at_{x}_{y}_{b}_{d}")

# Objective Function
model += contention_ub


# Solve the model
status = model.solve(PULP_CBC_CMD(msg=1))

# Print the results
print(f"status: {LpStatus[model.status]}")
print(f"objective: {model.objective.value()}")

for var in model.variables():
    print(f"{var.name}: {var.value()}")

for name, constraint in model.constraints.items():
    print(f"{name}: {constraint.value()}")

for b in range(B):

    G = nx.MultiDiGraph()

    for k,v in e_grid.items():
        if k[4] == b and type(v.value()) == float and v.value() > 0:
            G.add_edge(u_for_edge=(k[0],k[1]), v_for_edge=(k[2],k[3]), weight=k[5])
    for k,v in e_onramp.items():
        if k[2] == b and type(v.value()) == float and v.value() > 0:
            G.add_edge(u_for_edge=(k[0],k[1],"ce"), v_for_edge=(k[0],k[1]), weight=k[3])
    for k,v in e_offramp.items():
        if k[2] == b and type(v.value()) == float and v.value() > 0:
            G.add_edge(u_for_edge=(k[0],k[1]), v_for_edge=(k[0],k[1],"ce"), weight=k[3])

    pos = nx.spring_layout(G)

    # Draw the multigraph with offset edges
    plt.figure(figsize=(8, 6))
    draw_multigraph(G, pos)
    plt.title("Weighted, Directed Multigraph with Offset Edges")
    plt.axis('off')
    plt.show()


