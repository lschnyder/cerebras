import itertools
import matplotlib.pyplot as plt
import networkx as nx
from pulp import LpMinimize, LpProblem, LpStatus, lpSum, LpVariable, PULP_CBC_CMD
import numpy as np

def ceildiv(a : int, b : int):
    return -(a // -b)

# Define problem dimensions
M = 2
N = 2
B = 4

#Upper Bound on the sending cost
large_constant = M*N

# Create the problem
model = LpProblem(name="optimal-reduce", sense=LpMinimize)

# Define variables
V = {(x,y,b): LpVariable(name=f"({x},{y})_b:{b}", cat="Binary") for x in range(M) for y in range(N) for b in range(B)}
E = {(x,y,u,v,b): LpVariable(name=f"({x},{y}),({u},{v})_b:{b}", cat="Binary") for (x,y,b) in V.keys() for (u,v,c) in V.keys() if (x,y) != (u,v) and b==c}

# REVIEW: Maybe for length we can give a better upper bound
length = {(x,y,b): LpVariable(name=f"length_at_{v.name}", cat="Integer", lowBound=0, upBound=(M*N-1)*(M+N)) for (x,y,b),v in V.items()}
depth = {(x,y,b): LpVariable(name=f"depth_at_{v.name}", cat="Integer", lowBound=0, upBound=M*N-1) for (x,y,b),v in V.items()}

# Who sends how much
for (x,y,b) in V.keys():
    if (x,y) == (0,0):
        model += (lpSum([v for k,v in E.items() if k[0]==0 and k[1]==0 and k[4]==b]) == 0, f"root_never_sends{b}")
    else:
        model += (lpSum([v for k,v in E.items() if k[0]==x and k[1]==y and k[4]==b]) == 1, f"non-root_sends_once({x},{y},{b})") 

# Calculate length
for (x,y,b) in V.keys():
    if (x,y) == (0,0):
        model += (length[x,y,b] == 0, f"length({x},{y},{b})")
    else:
        # Encode the below constraint, ie, the length is the length of the next node plus the length to get there
        # model += (length[x,y,b] >= lpSum(E[x,y,u,v,b] * length[u,v] for (u,v) in V.keys() if (x,y) != (u,v)))
        min_length = 0
        max_length = (M*N-1)*(M+N)
        for (u,v,c) in V.keys():
            if c != b:
                continue
            if (x,y) != (u,v):
                model += (length[x,y,b] <= length[u,v,b] + abs(u-x) + abs(v-y) + max_length * (1 - E[x,y,u,v,b]), f"length_a({x},{y},{u},{v},{b})")
                model += (length[x,y,b] >= length[u,v,b] + abs(u-x) + abs(v-y) - max_length * (1 - E[x,y,u,v,b]), f"length_b({x},{y},{u},{v},{b})")
                # model += (length[x,y,b] <= max_length * E[x,y,u,v,b], f"length_a({x},{y},{u},{v},{b})")
                # model += (length[x,y,b] >= min_length * E[x,y,u,v,b], f"length_b({x},{y},{u},{v},{b})")
                # model += (length[x,y,b] <= length[u,v,b] + abs(u-x) + abs(v-y) - min_length * (1 - E[x,y,u,v,b]), f"length_c({x},{y},{u},{v},{b})")
                # model += (length[x,y,b] >= length[u,v,b] + abs(u-x) + abs(v-y) - max_length * (1 - E[x,y,u,v,b]), f"length_d({x},{y},{u},{v},{b})")

# Calculate depth
for (x,y,b) in V.keys():
    if (x,y) == (0,0):
        model += (depth[x,y,b] == 0, f"depth({x},{y},{b})")
    else:
        # Encode the below constraint, ie, the length is the length of the next node plus the length to get there
        # model += (length[x,y,b] >= lpSum(E[x,y,u,v,b] * length[u,v] for (u,v) in V.keys() if (x,y) != (u,v)))
        min_depth = 0
        max_depth = (M*N-1)
        for (u,v,c) in V.keys():
            if c != b:
                continue
            if (x,y) != (u,v):
                model += (depth[x,y,b] <= depth[u,v,b] + 1 + max_depth * (1 - E[x,y,u,v,b]), f"depth_a({x},{y},{u},{v},{b})")
                model += (depth[x,y,b] >= depth[u,v,b] + 1 - max_depth * (1 - E[x,y,u,v,b]), f"depth_b({x},{y},{u},{v},{b})")
                # model += (depth[x,y,b] <= max_depth * E[x,y,u,v,b], f"depth_a({x},{y},{u},{v},{b})")
                # model += (depth[x,y,b] >= min_depth * E[x,y,u,v,b], f"depth_b({x},{y},{u},{v},{b})")
                # model += (depth[x,y,b] <= depth[u,v,b] + 1 - min_depth * (1 - E[x,y,u,v,b]), f"depth_c({x},{y},{u},{v},{b})")
                # model += (depth[x,y,b] >= depth[u,v,b] + 1 - max_depth * (1 - E[x,y,u,v,b]), f"depth_d({x},{y},{u},{v},{b})")


# Contention
contention_ub = LpVariable(name="contention_ub", cat="Integer", lowBound=0, upBound=(M*N-1)*B)
for x in range(M):
    for y in range(N):
        c = lpSum([v for k,v in E.items() if k[2]==x and k[3]==y])
        model += (contention_ub >= c, f"contention_at_{x}_{y}")

# Energy
energy_ub = LpVariable(name="energy_ub", cat="Integer", lowBound=(M*N-1)*B, upBound=(M*N-1)*(M+N)*B)
model += (energy_ub >= lpSum([val*(abs(u-x) + abs(v-y)) for (x,y,u,v,b),val in E.items()]), "energy_ub")

# NumLinks
def get_available_links(M,N):
    if M < 1 or N < 1:
        return -1
    if M == 1 and N == 1:
        return 0
    if M == 1:
        return 2*(N-1)
    if N == 1:
        return get_available_links(N,M)
    return M*(N-1)*2 + (M-1)*N*2 

# Number of Links
link_is_used = {(x,y,u,v): LpVariable(f"link_is_used({x},{y})_({u},{v})", cat="Binary") for (x,y,u,v,b) in E.keys() if b == 0}
for (x,y,u,v),val in link_is_used.items():
    # model += (link_is_used[x,y,u,v] >= E[x,y,u,v,b], f"overapprox_link_is_used({x},{y})_({u},{v})_b:{b}") #REVIEW: Not sure if there are instances where we need this constraint
    model += (val <= lpSum(E[x,y,u,v,b] for b in range(B)), f"link_is_used_decision_a({x},{y})_({u},{v})")

num_links = LpVariable(name="num_links", cat="Integer", lowBound=M*N-1, upBound=get_available_links(M,N))
model += (num_links == lpSum([v for v in link_is_used.values()]), "num_links")

# Length
length_ub = LpVariable(name="length_ub", cat="Integer", lowBound=M*N-1, upBound=(M*N-1)*(M+N))
for v in length.values():
    model += (length_ub >= v, "length_ub_" + v.name)
    
# Depth
depth_ub = LpVariable(name="depth_ub", cat="Integer", lowBound=1, upBound=M*N-1)
for v in depth.values():
    model += (depth_ub >= v, "depth_ub_" + v.name)

# Encode E/N
div_max = M*N*B*B
div_min = ceildiv((M*N-1)*B,get_available_links(M,N))

div = LpVariable(name="div", cat="Integer", lowBound=div_min, upBound=div_max)

rest = LpVariable(name="div_rest", cat="Integer", lowBound=0, upBound=get_available_links(M,N)-1)
model += (rest <= num_links - 1, "constrain_rest")
b = {i: LpVariable(name=f"b_{i}", cat="Binary") for i in range(div_min, div_max+1)}
model += (lpSum([v for v in b.values()]) == 1, "sum_of_b_is_one")
model += (num_links == lpSum([v*k for k,v in b.items()]), "b_encodes_num_links")
for k,v in b.items():
    model += (energy_ub <= k*div + k*div_max*(1-b[k]) + rest, f"approx_div_a({k})")
    model += (energy_ub >= k*div - k*div_max*(1-b[k]) + rest, f"approx_div_b({k})")

    # # # model += (energy_ub <= k*(div_max+1)*b[k], f"approx_div_c({k})")
    # # # model += (energy_ub <= get_available_links(M,N)*(div_max+1)*b[k] + rest, f"approx_div_c({k})")
    # # # model += (energy_ub >= k*(div_min-1)*b[k] + rest, f"approx_div_d({k})")


# Obj max function
max_var = LpVariable(name="max", cat="Integer", lowBound=0, upBound=max([(M*N-1)*B, ceildiv((M*N-1)*(M+N)*B,get_available_links(M,N)) + (M*N-1)*(M+N)]))
model += (max_var >= contention_ub)
model += (max_var >= div + length_ub)

# # Objective Function
constant = 2*2+1
model += max_var + constant * depth_ub
# model += 2

model.writeLP("out.txt")
# Solve the model
status = model.solve(PULP_CBC_CMD(msg=1))

# Print the results
print(f"status: {LpStatus[model.status]}")
print(f"objective: {model.objective.value()}")

for var in model.variables():
    print(f"{var.name}: {var.value()}")



def grid_layout(G, rows, cols):
    pos = {}
    for i, node in enumerate(G.nodes()):
        row = i // cols
        col = i % cols
        pos[node] = (col, -row)  # Use -row to invert y-axis for a better visual layout
    return pos

# for name, constraint in model.constraints.items():
#     print(f"{name}: {constraint.value()}")

G = nx.DiGraph()

for k,v in V.items():
    if k[2] == 0:
        G.add_node((k[0],k[1]))

# pos = nx.spring_layout(G)
pos = grid_layout(G,M,N)

# Draw the multigraph with offset edges
plt.figure(figsize=(8, 8))

nx.draw_networkx_labels(G, pos, font_size=14, font_family='sans-serif')



import matplotlib.colors as mcolors
import random as rdm
# Get a list of all named colors
colors = ["blue","green","red","cyan","purple"]

c = 0.02
for b in range(B):
    for k,v in E.items():
        if k[4] == b and type(v.value()) == float and v.value() > 0:
            G.add_edge(u_of_edge=(k[0],k[1]), v_of_edge=(k[2],k[3]))
    nx.draw_networkx_edges(G, {k:(u+c*b,v+c*b) for k,(u,v) in pos.items()}, node_size=700, edge_color=colors[b])
    G.clear()
    for k,v in V.items():
        if k[2] == 0:
            G.add_node((k[0],k[1]))





plt.axis('off')
plt.show()


