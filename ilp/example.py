from scipy.optimize import linprog
from pulp import LpMaximize, LpProblem, LpStatus, lpSum, LpVariable
import numpy as np

model = LpProblem(name="resource-allocation", sense=LpMaximize)

# Define the decision variables
x = {i: LpVariable(name=f"x{i}", lowBound=0) for i in range(1, 5)}
y = {i: LpVariable(name=f"y{i}", cat="Binary") for i in (1, 3)}

# Add constraints
model += (lpSum(x.values()) <= 50, "manpower")
model += (3 * x[1] + 2 * x[2] + x[3] <= 100, "material_a")
model += (x[2] + 2 * x[3] + 3 * x[4] <= 90, "material_b")

M = 100
model += (x[1] <= y[1] * M, "x1_constraint")
model += (x[3] <= y[3] * M, "x3_constraint")
model += (y[1] + y[3] <= 1, "y_constraint")

# Set objective
model += 20 * x[1] + 12 * x[2] + 40 * x[3] + 25 * x[4]

# Solve the optimization problem
status = model.solve()

print(f"status: {model.status}, {LpStatus[model.status]}")
print(f"objective: {model.objective.value()}")

for var in model.variables():
    print(f"{var.name}: {var.value()}")

for name, constraint in model.constraints.items():
    print(f"{name}: {constraint.value()}")






# pulp EXAMPLE
# model = LpProblem(name="test-problem", sense=LpMaximize)

# # x = LpVariable(name="x", cat="Integer")
# # y = LpVariable(name="y", cat="Integer")
# x = LpVariable(name="x", cat="Binary")
# y = LpVariable(name="y", cat="Binary")

# # model += (2 * x + y <= 20, "red_constraint")
# # model += (4 * x - 5 * y >= -10, "blue_constraint")
# # model += (-x + 2 * y >= -2, "yellow_constraint")
# # model += (-x + 5 * y == 15, "green_constraint")
# model += (x - y >= 1, "crazy_constraint")

# obj_func = x + 2*y #== lpSum([x, 2*y])
# model += obj_func

# model.solve()
# print(model.status)
# print(model.objective.value())
# for var in model.variables():
#     print(f"{var.name}: {var.value()}")















# SCIPY TEST
# obj = [-1, -2]

# lhs_ineq = [[2,1],
#             [-4,5],
#             [1,-2]]

# rhs_ineq = [[20],
#             [10],
#             [2]]

# lhs_eq = [[-1,5]]

# rhs_eq = [[15]]

# bnd = [(0, np.inf),
#        (0, np.inf)]

# opt = linprog(c=obj, A_ub=lhs_ineq, b_ub=rhs_ineq, A_eq=lhs_eq, b_eq = rhs_eq, bounds=bnd,
#                 method="highs")

# print(opt)