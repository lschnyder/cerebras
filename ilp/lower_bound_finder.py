import itertools
import matplotlib.pyplot as plt
import networkx as nx
from pulp import LpMinimize, LpProblem, LpStatus, lpSum, LpVariable, PULP_CBC_CMD, GUROBI_CMD
import numpy as np
import pulp

def ceildiv(a : int, b : int):
    return -(a // -b)
# NumLinks
def get_available_links(M,N):
    if M < 1 or N < 1:
        return -1
    if M == 1 and N == 1:
        return 0
    if M == 1:
        return 2*(N-1)
    if N == 1:
        return get_available_links(N,M)
    return M*(N-1)*2 + (M-1)*N*2 

# Define problem dimensions
M = 6
N = 6
B = 100 #Assume B >= C, else just use less colors
C = 4 #Number of colors available

#Upper Bound on the sending cost
large_constant = M*N

def create_model():
    # Create the problem
    model = LpProblem(name="optimal-reduce", sense=LpMinimize)

    # Define variables
    V = {(x,y,c): LpVariable(name=f"({x},{y})_c_{c}", cat="Binary") for x in range(M) for y in range(N) for c in range(C)}
    E = {(x,y,u,v,c): LpVariable(name=f"({x},{y}),({u},{v})_c_{c}", cat="Binary") for (x,y,c) in V.keys() for (u,v,d) in V.keys() if (x,y) != (u,v) and c==d}

    # For now we assume all colors should have the same share of B, ie we don't optimize for minimum number of colors
    # color_size = {c: LpVariable(name=f"size_c:{c}", cat="Integer", lowBound=1, upBound=B) for c in range(C)}

    # REVIEW: Maybe for length we can give a better upper bound
    length = {(x,y,c): LpVariable(name=f"length_at_{v.name}", cat="Integer", lowBound=0, upBound=(M*N-1)*(M+N)) for (x,y,c),v in V.items()}
    depth = {(x,y,c): LpVariable(name=f"depth_at_{v.name}", cat="Integer", lowBound=0, upBound=M*N-1) for (x,y,c),v in V.items()}

    # Who sends how much
    for (x,y,c) in V.keys():
        if (x,y) == (0,0):
            model += (lpSum([v for k,v in E.items() if k[0]==0 and k[1]==0 and k[4]==c]) == 0, f"root_never_sends{c}")
        else:
            model += (lpSum([v for k,v in E.items() if k[0]==x and k[1]==y and k[4]==c]) == 1, f"non-root_sends_once({x},{y},{c})") 

    # Calculate length
    for (x,y,c) in V.keys():
        if (x,y) == (0,0):
            model += (length[x,y,c] == 0, f"length({x},{y},{c})")
        else:
            # Encode the below constraint, ie, the length is the length of the next node plus the length to get there
            # model += (length[x,y,b] >= lpSum(E[x,y,u,v,b] * length[u,v] for (u,v) in V.keys() if (x,y) != (u,v)))
            min_length = 0
            max_length = (M*N-1)*(M+N)
            for (u,v,d) in V.keys():
                if d != c:
                    continue
                if (x,y) != (u,v):
                    model += (length[x,y,c] <= length[u,v,c] + abs(u-x) + abs(v-y) + max_length * (1 - E[x,y,u,v,c]), f"length_a({x},{y},{u},{v},{c})")
                    model += (length[x,y,c] >= length[u,v,c] + abs(u-x) + abs(v-y) - max_length * (1 - E[x,y,u,v,c]), f"length_b({x},{y},{u},{v},{c})")
                    # model += (length[x,y,b] <= max_length * E[x,y,u,v,b], f"length_a({x},{y},{u},{v},{b})")
                    # model += (length[x,y,b] >= min_length * E[x,y,u,v,b], f"length_b({x},{y},{u},{v},{b})")
                    # model += (length[x,y,b] <= length[u,v,b] + abs(u-x) + abs(v-y) - min_length * (1 - E[x,y,u,v,b]), f"length_c({x},{y},{u},{v},{b})")
                    # model += (length[x,y,b] >= length[u,v,b] + abs(u-x) + abs(v-y) - max_length * (1 - E[x,y,u,v,b]), f"length_d({x},{y},{u},{v},{b})")

    # Calculate depth
    for (x,y,c) in V.keys():
        if (x,y) == (0,0):
            model += (depth[x,y,c] == 0, f"depth({x},{y},{c})")
        else:
            # Encode the below constraint, ie, the length is the length of the next node plus the length to get there
            # model += (length[x,y,b] >= lpSum(E[x,y,u,v,b] * length[u,v] for (u,v) in V.keys() if (x,y) != (u,v)))
            min_depth = 0
            max_depth = (M*N-1)
            for (u,v,d) in V.keys():
                if d != c:
                    continue
                if (x,y) != (u,v):
                    model += (depth[x,y,c] <= depth[u,v,c] + 1 + max_depth * (1 - E[x,y,u,v,c]), f"depth_a({x},{y},{u},{v},{c})")
                    model += (depth[x,y,c] >= depth[u,v,c] + 1 - max_depth * (1 - E[x,y,u,v,c]), f"depth_b({x},{y},{u},{v},{c})")
                    # model += (depth[x,y,b] <= max_depth * E[x,y,u,v,b], f"depth_a({x},{y},{u},{v},{b})")
                    # model += (depth[x,y,b] >= min_depth * E[x,y,u,v,b], f"depth_b({x},{y},{u},{v},{b})")
                    # model += (depth[x,y,b] <= depth[u,v,b] + 1 - min_depth * (1 - E[x,y,u,v,b]), f"depth_c({x},{y},{u},{v},{b})")
                    # model += (depth[x,y,b] >= depth[u,v,b] + 1 - max_depth * (1 - E[x,y,u,v,b]), f"depth_d({x},{y},{u},{v},{b})")


    # Contention
    contention_ub = LpVariable(name="contention_ub", cat="Integer", lowBound=0, upBound=(M*N-1)*B)
    for x in range(M):
        for y in range(N):
            d = lpSum([v for k,v in E.items() if k[2]==x and k[3]==y]) * B/C
            model += (contention_ub >= d, f"contention_at_{x}_{y}")

    # Energy
    energy_ub = LpVariable(name="energy_ub", cat="Integer", lowBound=(M*N-1)*B, upBound=(M*N-1)*(M+N)*B)
    model += (energy_ub >= lpSum([val*(abs(u-x) + abs(v-y)) for (x,y,u,v,c),val in E.items()]) * B/C, "energy_ub")


    # Number of Links
    link_is_used = {(x,y,u,v): LpVariable(f"link_is_used({x},{y})_({u},{v})", cat="Binary") for (x,y,u,v,c) in E.keys() if c == 0}
    for (x,y,u,v),val in link_is_used.items():
        # model += (link_is_used[x,y,u,v] >= E[x,y,u,v,b], f"overapprox_link_is_used({x},{y})_({u},{v})_b:{b}") #REVIEW: Not sure if there are instances where we need this constraint
        model += (val <= lpSum(E[x,y,u,v,c] for c in range(C)), f"link_is_used_decision_a({x},{y})_({u},{v})")

    num_links = LpVariable(name="num_links", cat="Integer", lowBound=M*N-1, upBound=get_available_links(M,N))
    model += (num_links == lpSum([v for v in link_is_used.values()]), "num_links")

    # Length
    length_ub = LpVariable(name="length_ub", cat="Integer", lowBound=M*N-1, upBound=(M*N-1)*(M+N))
    for v in length.values():
        model += (length_ub >= v, "length_ub_" + v.name)
        
    # Depth
    depth_ub = LpVariable(name="depth_ub", cat="Integer", lowBound=1, upBound=M*N-1)
    for v in depth.values():
        model += (depth_ub >= v, "depth_ub_" + v.name)

    # Encode E/N
    div_max = M*N*B*B
    div_min = ceildiv((M*N-1)*B,get_available_links(M,N))

    div = LpVariable(name="div", cat="Integer", lowBound=div_min, upBound=div_max)

    rest = LpVariable(name="div_rest", cat="Integer", lowBound=0, upBound=get_available_links(M,N)-1)
    model += (rest <= num_links - 1, "constrain_rest")
    q = {i: LpVariable(name=f"q_{i}", cat="Binary") for i in range(M*N-1, get_available_links(M,N)+1)}
    model += (lpSum([v for v in q.values()]) == 1, "sum_of_q_is_one")
    model += (num_links == lpSum([v*k for k,v in q.items()]), "q_encodes_num_links")
    for k,v in q.items():
        model += (energy_ub <= k*div + get_available_links(M,N)*div_max*(1-q[k]) + rest, f"approx_div_a({k})")
        model += (energy_ub >= k*div - get_available_links(M,N)*div_max*(1-q[k]) + rest, f"approx_div_b({k})")

        # # # model += (energy_ub <= k*(div_max+1)*b[k], f"approx_div_c({k})")
        # # # model += (energy_ub <= get_available_links(M,N)*(div_max+1)*b[k] + rest, f"approx_div_c({k})")
        # # # model += (energy_ub >= k*(div_min-1)*b[k] + rest, f"approx_div_d({k})")


    # Obj max function
    max_var = LpVariable(name="max_var", cat="Integer", lowBound=0)
    model += (max_var >= contention_ub)
    model += (max_var >= div)
    model += (max_var >= length_ub)
    constant = 2*2+1
    model += (max_var >= constant * depth_ub)

    # # Objective Function
    model += max_var
    # model += 2
    return model

# model.writeLP("out.txt")
# Solve the model

# time_limit = 12*60*60 # 12 hours
time_limit = 2 * 60
# solver = GUROBI_CMD(msg=True, timeLimit=time_limit)

# # Print the results
# print(f"status: {LpStatus[model.status]}")
# print(f"objective: {model.objective.value()}")

# with open("M:"+str(M)+","+"N:"+str(N)+","+"B:"+str(B)+","+"C:"+str(C)+".txt", "a") as f:
    
#     for var in model.variables():
#         if var.name in [v.name for v in E.values()]:
#             print(f"{var.name}: {var.value()}", file=f)



import os
import pickle

# Function to save intermediate results
def save_solution(problem, filename):
    with open(filename, 'wb') as f:
        pickle.dump({v.name: v.value() for v in problem.variables()}, f)

# Function to load intermediate results
def load_solution(filename):
    with open(filename, 'rb') as f:
        return pickle.load(f)
    

prob = create_model()
# Check if an intermediate solution exists
if os.path.exists(f'temp_sol_{M}_{N}_{B}_{C}.pkl'):
    # Load the intermediate solution
    variable_values = load_solution(f'temp_sol_{M}_{N}_{B}_{C}.pkl')
    solver = pulp.GUROBI_CMD(timeLimit=time_limit,warmStart=True)

    print(variable_values)

    # Set the variables to the loaded values
    for v in prob.variables():
        if v.name in variable_values:
            v.varValue = variable_values[v.name]
            
else:
    # Define the ILP problem as before if no intermediate solution exists
    solver = pulp.GUROBI_CMD(timeLimit=time_limit)

# Solve the problem again
prob.solve(solver)

print(prob.status)
# Check the solver status
if prob.status in [pulp.LpStatusOptimal, pulp.LpStatusInfeasible, pulp.LpStatusUnbounded]:
    # Solver finished within the time limit
    print("Solver finished within the time limit.")
    with open(f"final_solution_{M}_{N}_{B}_{C}.txt", 'w') as f:
        f.write(f"{prob.objective.value()}")
else:
    # Solver did not finish, save intermediate results
    print("Solver did not finish, saving intermediate results.")
    save_solution(prob, f'temp_sol_{M}_{N}_{B}_{C}.pkl')

def grid_layout(G, rows, cols):
    pos = {}
    for i, node in enumerate(G.nodes()):
        row = i // cols
        col = i % cols
        pos[node] = (col, -row)  # Use -row to invert y-axis for a better visual layout
    return pos

# for name, constraint in model.constraints.items():
#     print(f"{name}: {constraint.value()}")


def plot_figure(V):
    G = nx.DiGraph()

    for k,v in V.items():
        if k[2] == 0:
            G.add_node((k[0],k[1]))

    # pos = nx.spring_layout(G)
    pos = grid_layout(G,M,N)

    # Draw the multigraph with offset edges
    plt.figure(figsize=(8, 8))

    nx.draw_networkx_labels(G, pos, font_size=14, font_family='sans-serif')



    import matplotlib.colors as mcolors
    import random as rdm
    # Get a list of all named colors
    colors = ["blue","green","red","black","purple"]

    d = 0.02
    for c in range(C):
        for k,v in E.items():
            if k[4] == c and type(v.value()) == float and v.value() > 0:
                G.add_edge(u_of_edge=(k[0],k[1]), v_of_edge=(k[2],k[3]))
        nx.draw_networkx_edges(G, {k:(u+d*c,v+d*c) for k,(u,v) in pos.items()}, node_size=700, edge_color=colors[c])
        G.clear()
        for k,v in V.items():
            if k[2] == 0:
                G.add_node((k[0],k[1]))





    plt.axis('off')
    # plt.show()
    plt.savefig("M:"+str(M)+","+"N:"+str(N)+","+"B:"+str(B)+","+"C:"+str(C)+".png")
