
//
// A collective communication: receive data from cmd fan from left,
// setups a fitler to retrieve data via MEMCPYH2D_DATA
// There are two modes:
// - memcpy mode: copy data to device memory
// - streaming mode: transfer data from MEMCPYH2D_DATA to user's H2D color,
//   the user can receive the data by WTT or microthread explicitly
//
// The caller must instantiate memcpyh2d module with H2D colors (one or more),
// memcpyh2d generates the initial routing and filter at comptime, and
// h2d_configure() reconfigures the filter at runtime
//

//
// It uses a counter filter with the following routing on MEMCPYH2D_DATA
//
// demux  -->-->   --->-->   -->-->  -->
//          |        |        |       |
//          V        V        V       V
//
// The data from demux broadcasts to a row PEs
// Each PE must set up the counter filter before the data arrives
//
// Here are three examples and corrsponding setting of the counter filter
// (layout.csl chooses example 2, round-robin wavelet along row PEs)
//
// Example 1: cliff distribution
//   order of tensor x[w][l]
//   PE0: [0,     T)
//   PE1: [T,   2*T)
//   PE2: [2*T, 3*T)
//
// limit = width*T - 1
// PE pid filters [T*pid, T*(pid+1))
// init_count = (limit+1 - pid*T) % (limit+1)
// range [0, max_pass] where max_pass = LOCAL_OUT_SZ-1
//
// Example 2: round-robin a wavelet along row PEs
//    order of tensor: x[l][w]
//    PE0: 0, w  , 2*w  , ...
//    PE1: 1, w+1, 2*w+1, ...
//    PE2: 2, w+2, 2*w+2, ...
//
//  l = 4, w = 4
//  data from demux  x0  x4  x8 x12  x1  x5  x9 x13  x2  x6 x10 x14  x3  x7 x11 x15
//  P0:               0   1   2   3   0   1   2   3   0   1   2   3   0   1   2   3
//  P1:               3   0   1   2   3   0   1   2   3   0   1   2   3   0   1   2
//  P2:               2   3   0   1   2   3   0   1   2   3   0   1   2   3   0   1
//  P3:               1   2   3   0   1   2   3   0   1   2   3   0   1   2   3   0
//
// limit = width - 1
// PE pid filters [pid, (pid+1))
// init_count = (limit+1 - pid) % (limit+1)
// range [0, max_pass] where max_pass = 0
//
// Example 3: round-robin chunk size along row PEs
//    order of tensor: x[l/chunk][w][chunk]
//    PE0: [0, chunk), w*chunk+[0, chunk)  , 2*w*chunk+[0, chunk)  , ...
//    PE1: [chunk, 2*chunk), w*chunk+[chunk, 2*chunk)  , 2*w*chunk+[chunk, 2*chunk)  , ...
//    PE2: [2*chunk, 3*chunk), w*chunk+[2*chunk, 3*chunk)  , 2*w*chunk+[2*chunk, 3*chunk)  , ...
//
// limit = width*chunk - 1
// PE pid filters [pid*chunk, (pid+1)*chunk)
// init_count = (limit+1 - pid*chunk) % (limit+1)
// range [0, max_pass] where max_pass = chunk-1
//
//

param MEMCPYH2D_DATA: color ;  // system-reserved for memcpyH2D

// if the user only has one H2D color, then USER_MEMCPYH2D_DATA_[2|3|4]=32, so
// we don't generate routing for USER_MEMCPYH2D_DATA_[2|3|4]
param USER_MEMCPYH2D_DATA_1: color = @get_color(32); // 1-th H2D
param USER_MEMCPYH2D_DATA_2: color = @get_color(32); // 2-th H2D
param USER_MEMCPYH2D_DATA_3: color = @get_color(32); // 3-th H2D
param USER_MEMCPYH2D_DATA_4: color = @get_color(32); // 4-th H2D
//---

// WARNING: input queue only uses in memcpy, not streaming
// the input queue binds to fabin, to read data from the filter
// streaming gives the control back to the user who can use either WTT or microthread
// to process the data from the filter
param input_queue_id: i16; // the caller has to reserve this input queue for memcpyH2D
// the current proposal is to use input queue 0 and output queue 0
param output_queue_id: i16; // the caller has to reserve this output queue for memcpyH2D
//---

// explicit DSR allocation
param dest_dsr_ids: [1]u16;
param src1_dsr_ids: [1]u16;

param first_pe: bool; // (0 == px);
param last_pe: bool;  // ((width-1) == px);
param width : i16 ;

param RXACT_CMD: color; // RXCOMMAND=C22
param SYS_UBLK_C22: local_task_id; // an entrypoint to unblock C22


// define a pseudo task id to RXACT_CMD
// neither WTT nor input queue binds to PSEUDO_WTT_RXACT_CMD
// a workaround to avoid compilation warning on @unblock(RXACT_CMD)
const PSEUDO_WTT_RXACT_CMD: data_task_id = @get_data_task_id(RXACT_CMD);

//--------------------- system utility for teardown

const tile_config = @import_module("<tile_config>");

const color_config = tile_config.color_config;
const memcpyh2d_addr = color_config.get_color_config_addr(MEMCPYH2D_DATA);

//-------------------

const fabric = @import_module("<layout>");
fn get_x_coord() u16 {
    return fabric.get_x_coord();
}
fn get_y_coord() u16 {
    return fabric.get_y_coord();
}


// runtime decides is_streaming
//    is_streaming == !(c >= 24)
// if the command is memcpyh2d(c=24,...), then it uses memcpy
// if the command is memcpyh2d(c=1,...), then it uses memcpy streaming
//
// memcpy streaming: 0 <= c < 24
// memcpy: c = 31
// return 0: memcpy
//        1: streaming
fn is_streaming_eval( user_color: u16) i16 {
    if (user_color < 24){
        return 1;
    }else{
        @assert(31 == user_color);
        return 0;
    }
}

////////////////////////////////////////////////////////////////////////////////
// Main memory (48KB)
////////////////////////////////////////////////////////////////////////////////


// Number of receivers for the next tensor, corresponds to filter limit
var num_receivers: i16 = 0;

// Offramp output direction bit for color
var offramp_bit: u16 = 0x0010;

// Counter init value for the next RXACT tensor. This value is equal to the
// PE's index in the order of receivers. If the PE does not receive any data
// then this value will be 0xffff.
var receiver_idx: i16 = -1; // 0xffff;

////////////////////////////////////////////////////////////////////////////////
// DSDs
// data-structure descriptors (DSDs), loaded into data-structure registers (DSRs)
//
// Queues 0,1: input depth 6 wavelets
// Queues 2,3: input depth 4 wavelets
// Queues 4-7: input depth 2 wavelets
//
// queues 0,1: output depth 2 wavelets
// queues 2,3: output depth 6 wavelets
// queues 4,5: output depth 2 wavelets
//
// Length of an operand:
// The length of all other types of DSRs is specified by the length field of its DSD. When
// the bits encoding the length are 0x7fff, the length is infinite.
//
// Length of the vector instruction is then determined in the following order:
// 1. If src0 has a non-zero length, that length is used
// 2. If src1 has a non-zero length, that length is used
// 3. If dst has a non-zero length, that length is used
// 4. if no operands have length (all operands are GPR), length = 1
////////////////////////////////////////////////////////////////////////////////

const dest_dsr = @get_dsr(dsr_dest, dest_dsr_ids[0]);
const src1_dsr = @get_dsr(dsr_src1, src1_dsr_ids[0]);

// To change the base address and the length of a DSD, csl requires a dummy DSD.
// The type here doesn't matter
const dummy = @zeros([1]i16);
// The length doesn't matter either since csl will overwrite it
const dummy_dsd = @get_dsd(mem1d_dsd, .{.tensor_access = |i|{42} -> dummy[i]});


// input queue = 0 must be reserved, the user cannot use input queue 0
//
// WARNING: lenghth = 1 is don't care, reset length at runtime
// cslang does not allow "extent = 0"
var fab_recv_wdsd = @get_dsd(fabin_dsd, .{
    .extent = 1,
    .fabric_color = MEMCPYH2D_DATA,
    .input_queue = @get_input_queue(input_queue_id)
});

//--- TODO 4: cslang can support runtime fabric_color
// output queue = 0 must be reservd, the user cannot use output queue 0
//
// WARNING: lenghth = 1 is don't care, reset length at runtime
// cslang does not allow "extent = 0"
var fab_trans_wdsd_1 = @get_dsd(fabout_dsd, .{
    .extent = 1,
    .fabric_color = USER_MEMCPYH2D_DATA_1,
    .output_queue = @get_output_queue(output_queue_id)
});

var fab_trans_wdsd_2 = @get_dsd(fabout_dsd, .{
    .extent = 1,
    .fabric_color = USER_MEMCPYH2D_DATA_2,
    .output_queue = @get_output_queue(output_queue_id)
});

var fab_trans_wdsd_3 = @get_dsd(fabout_dsd, .{
    .extent = 1,
    .fabric_color = USER_MEMCPYH2D_DATA_3,
    .output_queue = @get_output_queue(output_queue_id)
});

var fab_trans_wdsd_4 = @get_dsd(fabout_dsd, .{
    .extent = 1,
    .fabric_color = USER_MEMCPYH2D_DATA_4,
    .output_queue = @get_output_queue(output_queue_id)
});
//---

// memcpyh2d_start() is called if the PE in an active row.
// memcpy (not memcpy streaming) calls memcpyh2d_start()
fn memcpyh2d_start( d_A : u16, pe_length : u16, is_half: u16 ) void {

    // (1) reset the length of fabin
    fab_recv_wdsd = @set_dsd_length(fab_recv_wdsd, pe_length);

    // (2) reset base address, offset and length
    // TODO: we may add offset in the future in order to copy host tensor on A[offset]
    var mem_x_buf_dsd = @set_dsd_base_addr(dummy_dsd, @get_tensor_ptr(d_A));
    // mem_x_buf_dsd = @increment_dsd_offset(mem_x_buf_dsd, offset, f32);
    mem_x_buf_dsd = @set_dsd_length(mem_x_buf_dsd, pe_length);
    @load_to_dsr(dest_dsr, mem_x_buf_dsd);
    @load_to_dsr(src1_dsr, fab_recv_wdsd, .{.async=true, .unblock=SYS_UBLK_C22} );
    if (0 < is_half){
        @mov16(dest_dsr, src1_dsr, .{.async=true} );
    }else{
        @mov32(dest_dsr, src1_dsr, .{.async=true} );
    }
    // WARNING: no need to unblock C22 when H2D finishes.
    // cmd fan will send a TD after the data, so T29 will turn C21 back to teardown mode
    // and activate SYS_UBLK_C22 which unblocks C22.
}


fn unblock_cmd_stream() void {
    @unblock( PSEUDO_WTT_RXACT_CMD );
}


// The caller has blocked SYS_UBLK_C22
//
// case 1: inactive row
//  - keep C21 in teardown mode (do NOT clear TIP)
//  - unblock C22 to proceed next command because cmd fan does not send TD to this row,
//    so we must unblock C22 here
//    Also unblock SYS_UBLK_C22 because no H2D operation here
//
// case 2:  active row and inactive PE
//  - remove offramp
//  - clear TIP
//  - do NOT unblock C22 because cmd fan sends a TD
//  - unblock SYS_UBLK_C22 because no H2D operation here
//
// case 3: active row and active PE
//  - reconfigure filter
//  - clear TIP
//  - memcpy: call memcpyh2d_start()
//  - streaming: forward data to corresponding user's H2D color
//  - do NOT unblock C22 because cmd fan sends a TD
//  - unblock SYS_UBLK_C22 when UT0 finishes
//
fn h2d_configure(
    memcpyH2D_c: i16, // user's H2D color
    px: i16,
    py: i16,
    w: i16,
    h: i16,
    d_A: u16, // d_A is a symbol
    pe_length: u16,
    is_half: u16) void
{
    const _px = @as(i16, get_x_coord());
    const _py = @as(i16, get_y_coord());
    const offramp = color_config.fabric_io.TX_RAMP;

    // region of interest ((px, py), (w, h))
    // inactive row: _py is outside [py, py+h)
    // inactive PE: receiver_idx = -1
    // active PE:  receiver_idx = [0, w)
    receiver_idx = (_px - px);
    if (receiver_idx < 0) {
        receiver_idx = -1;
    }
    if (receiver_idx >= w){
        receiver_idx = -1;
    }
    // if PEs are outside [py, py+h-1], cmd fan does not send data or TD,
    // we must keep C21 in teardown mode and unblock C22 explicitly to proceed
    // next command.
    var inactive_row: bool = false;
    if ( _py < py ){
        inactive_row = true;
    }
    if ( _py >= (py+h) ){
        inactive_row = true;
    }

    if (inactive_row){
        // case 1: inactive row
        // keep C21 in teardown mode, DO NOT clear TIP
        // unblock C22 to proceed the next command (cmd fan does not send a TD to inactive row)
        unblock_cmd_stream();
        // WARNING: SYS_UBLK_C22 is blocked when H2D is received.
        // unblock it because of no H2D operation here.
        @unblock(SYS_UBLK_C22);
    }else{
        // active row
        if ( -1 == receiver_idx ){
            // case 2:  active row and inactive PE

            // PE does not receive data, remove offramp connection
            color_config.clear_io_direction(memcpyh2d_addr, offramp);

            // clear teardown-in-progress bit
            tile_config.teardown.exit(MEMCPYH2D_DATA);

            // do NOT unblock C22 because cmd fan sends a TD to activate SYS_UBLK_C22
            // which unblocks C22
            @unblock(SYS_UBLK_C22);
        }else{
            // case 3: active row and active PE
            const filter_id = @get_filter_id(MEMCPYH2D_DATA);

            // ".max_counter = 0"
            // SDR removes the "limits" on the counter filter, instead, we need to set
            // the pass_cnt and drop_cnt. To be compatible with old API which still uses
            // "limits", we need to set pass_cnt (in this case, max_counter) before
            // calling set_max_counter(). Otherwise the drop_cnt may not be set correctly.
            tile_config.filters.set_max_counter(filter_id, 0);

            num_receivers = w;
            // num_receivers = # of active receviers
            // limit1 = (num_receivers-1), similar to limit1 = N-1 for 1st teardown
            tile_config.filters.set_active_limit(filter_id, @bitcast(u16, num_receivers-1));

            // init_count = (num_receivers - receiver_idx) % num_receivers
            // so if receiver_idx = 0, then init_count = 0
            // Here receiver_idx != -1
            var r_tmp : i16 = num_receivers;
            var r_state : i16 = receiver_idx;
            r_tmp = r_tmp - r_state;
            if (0 == r_state){
                r_tmp = 0;
            }
            tile_config.filters.set_counter(filter_id, @bitcast(u16,r_tmp));

            // enable OFFRAMP
            color_config.set_io_direction(memcpyh2d_addr, offramp);

            // Clear teardown-in-progress bit on receive color
            // config_reg[c] ^= mask where mask = 1 << 14
            tile_config.teardown.exit(MEMCPYH2D_DATA);

            // (3) receive data by calling memcpyh2d_start
            if (0 == is_streaming_eval(@as(u16,memcpyH2D_c))){
                // a copy with microthread
                // SYS_UBLK_C22 is unblocked when UT0 finishes the copy
                memcpyh2d_start( d_A, pe_length, is_half ); // callback = f_sys_exit
            }else{
                fab_recv_wdsd = @set_dsd_length(fab_recv_wdsd, pe_length); // binds to internal MEMCPYH2D_DATA

// WARNING: It is not legal to use a normal-mode fabric input vector operand in a microthreaded
// instruction. For example, a combination where src0 is a normal mode fabric vector and
// dst is a microthreaded fabric vector is illegal
//    @load_to_dsr(src1_dsr, fab_recv_wdsd) can stall RTM backward phase
//
// Both fabin and fabout must have UE = 1 (microthread mode)

                @load_to_dsr(src1_dsr, fab_recv_wdsd, .{.async=true} );
                if ( (@get_int(USER_MEMCPYH2D_DATA_1) <24) and (@get_int(USER_MEMCPYH2D_DATA_1) == @as(u16,memcpyH2D_c)) ){
                    fab_trans_wdsd_1 = @set_dsd_length(fab_trans_wdsd_1, pe_length);
                    @load_to_dsr(dest_dsr, fab_trans_wdsd_1, .{.async=true, .unblock=SYS_UBLK_C22});
                    @mov32(dest_dsr, src1_dsr, .{.async=true} );

                }else if ( (@get_int(USER_MEMCPYH2D_DATA_2) <24) and (@get_int(USER_MEMCPYH2D_DATA_2) == @as(u16,memcpyH2D_c)) ){
                    fab_trans_wdsd_2 = @set_dsd_length(fab_trans_wdsd_2, pe_length);
                    @load_to_dsr(dest_dsr, fab_trans_wdsd_2, .{.async=true, .unblock=SYS_UBLK_C22} );
                    @mov32(dest_dsr, src1_dsr, .{.async=true} );

                }else if ( (@get_int(USER_MEMCPYH2D_DATA_3) <24) and (@get_int(USER_MEMCPYH2D_DATA_3) == @as(u16,memcpyH2D_c)) ){
                    fab_trans_wdsd_3 = @set_dsd_length(fab_trans_wdsd_3, pe_length);
                    @load_to_dsr(dest_dsr, fab_trans_wdsd_3, .{.async=true, .unblock=SYS_UBLK_C22} );
                    @mov32(dest_dsr, src1_dsr, .{.async=true} );

                }else if ( (@get_int(USER_MEMCPYH2D_DATA_4) <24) and (@get_int(USER_MEMCPYH2D_DATA_4) == @as(u16,memcpyH2D_c)) ){
                    fab_trans_wdsd_4 = @set_dsd_length(fab_trans_wdsd_4, pe_length);
                    @load_to_dsr(dest_dsr, fab_trans_wdsd_4, .{.async=true, .unblock=SYS_UBLK_C22} );
                    @mov32(dest_dsr, src1_dsr, .{.async=true} );
                }
                // WARNING: no need to unblock C22 when H2D finishes.
                // cmd fan will send a TD after the data, so T29 will turn C21 back to teardown mode
                // and activate SYS_UBLK_C22 which unblocks C22.
            }
        }// if an active PE
    }// active row

}


comptime {

    // WARNING: internal color MEMCPYH2D_DATA receives the data with a microthread
    // both memcpy and memcpy streaming
    // so we need to block this color
    @block(MEMCPYH2D_DATA);

//--- TODO 6: the user must not configure the routing of USER_MEMCPYH2D_DATA_j
// if USER_MEMCPYH2D_DATA_j is invalid (32), then csl does not generate the routing
    if (@get_int(USER_MEMCPYH2D_DATA_1) < 24){
        @set_local_color_config(USER_MEMCPYH2D_DATA_1, .{ .routes = .{ .rx = .{RAMP}, .tx = .{RAMP} } } );
    }
    if (@get_int(USER_MEMCPYH2D_DATA_2) < 24){
        @set_local_color_config(USER_MEMCPYH2D_DATA_2, .{ .routes = .{ .rx = .{RAMP}, .tx = .{RAMP} } } );
    }
    if (@get_int(USER_MEMCPYH2D_DATA_3) < 24){
        @set_local_color_config(USER_MEMCPYH2D_DATA_3, .{ .routes = .{ .rx = .{RAMP}, .tx = .{RAMP} } } );
    }
    if (@get_int(USER_MEMCPYH2D_DATA_4) < 24){
        @set_local_color_config(USER_MEMCPYH2D_DATA_4, .{ .routes = .{ .rx = .{RAMP}, .tx = .{RAMP} } } );
    }
//---
}


//
// routing of internal MEMCPYH2D_DATA (broadcasting to a row PEs)
// -->-->   --->-->   -->-->  -->
//    |        |        |       |
//    V        V        V       V
//
// Example 1: Cliff distribution
//   order of tensor x[w][l]
//   PE0: [0,     T)
//   PE1: [T,   2*T)
//   PE2: [2*T, 3*T)
//
// limit = width*T - 1
// PE pid filters [T*pid, T*(pid+1))
// init_count = (limit+1 - pid*T) % (limit+1)
// range [0, max_pass] where max_pass = LOCAL_OUT_SZ-1
//
// Example 2: round-robin along row PEs
//    order of tensor: x[l][w]
//    PE0: 0, w  , 2*w  , ...
//    PE1: 1, w+1, 2*w+1, ...
//    PE2: 2, w+2, 2*w+2, ...
//
//  l = 4, w = 4
//  data from demux  x0  x4  x8 x12  x1  x5  x9 x13  x2  x6 x10 x14  x3  x7 x11 x15
//  P0:               0   1   2   3   0   1   2   3   0   1   2   3   0   1   2   3
//  P1:               3   0   1   2   3   0   1   2   3   0   1   2   3   0   1   2
//  P2:               2   3   0   1   2   3   0   1   2   3   0   1   2   3   0   1
//  P3:               1   2   3   0   1   2   3   0   1   2   3   0   1   2   3   0
//
// limit = width - 1
// PE pid filters [pid, (pid+1))
// init_count = (limit+1 - pid) % (limit+1)
// range [0, max_pass] where max_pass = 0
//
// Example 3: round-robin chunk size along row PEs
//    order of tensor: x[l/chunk][w][chunk]
//    PE0: [0, chunk), w*chunk+[0, chunk)  , 2*w*chunk+[0, chunk)  , ...
//    PE1: [chunk, 2*chunk), w*chunk+[chunk, 2*chunk)  , 2*w*chunk+[chunk, 2*chunk)  , ...
//    PE2: [2*chunk, 3*chunk), w*chunk+[2*chunk, 3*chunk)  , 2*w*chunk+[2*chunk, 3*chunk)  , ...
//
// limit = width*chunk - 1
// PE pid filters [pid*chunk, (pid+1)*chunk)
// init_count = (limit+1 - pid*chunk) % (limit+1)
// range [0, max_pass] where max_pass = chunk-1
//
comptime {
    var limit_plus_1 :i16 = width;

    // the filter is reconfigured for every tensor.
    // the teardown wavelet ignores the filter, and triggers task 29.
    // the comptime setting of the filter is don't care
    // the original setting is
    // ---
    //   .init_counter = (limit_plus_1 - _px) % limit_plus_1,
    // ---
    const baseConfig = .{
        .routes = .{ .rx = .{ WEST }, .tx = .{RAMP, EAST} },
        .filter = .{
            .kind = .{ .counter = true },
            .count_data = true,
            .count_control = false,
            .init_counter = 0,
            .max_counter  = 0,
            .limit1 = (limit_plus_1 - 1)
        },
        .teardown = true
    };

    const baseConfig_last = .{
        .routes = .{ .rx = .{ WEST }, .tx = .{ RAMP } },
        .filter = .{
            .kind = .{ .counter = true },
            .count_data = true,
            .count_control = false,
            .init_counter = 0,
            .max_counter  = 0,
            .limit1 = (limit_plus_1 - 1)
        },
        .teardown = true
    };

    if ( !last_pe ){
        // px < (w-1)
        @set_local_color_config(MEMCPYH2D_DATA, baseConfig );
    }else{
        @set_local_color_config(MEMCPYH2D_DATA, baseConfig_last );
    }
}

// binding a color to an input queue.
// This is necessary when an explicit DSR binds to a fabin DSD because
// the compiler no longer can generate the instruction to set up the
// config register of input queue.
comptime {
    @initialize_queue(@get_input_queue(input_queue_id), .{.color = MEMCPYH2D_DATA} );
}
