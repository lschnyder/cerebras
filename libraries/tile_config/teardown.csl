param target : imported_module;

const addresses = target.addresses;
const reg_type = target.reg_type;
const reg_ptr = target.reg_ptr;
const double_reg_type = target.double_reg_type;
const word_size = target.word_size;

// Teardown functions.
//////////////////////

// Returns the task ID that is reserved for the teardown handler.
fn get_task_id() local_task_id {
  return @get_local_task_id(29);
}

// Return the values of the "teardown-pending" registers combined into
// one value. Only the first invocation of this function per-task is
// guaranteed to return the correct value. Any additional calls per-task
// will have undefined results.
fn get_pending() double_reg_type {
  const lo_bits = @bitcast(reg_ptr, @as(reg_type, addresses.TEARDOWN_PENDING)).*;
  const hi_bits = @bitcast(reg_ptr, @as(reg_type, addresses.TEARDOWN_PENDING) + word_size).*;
  return (@as(double_reg_type, hi_bits) << (word_size * 8)) |
          @as(double_reg_type, lo_bits);
}

// Given a value that represents the "teardown-pending" state, which has 1 bit
// per routable color indicating the ones that are currently in teardown state,
// return `true` iff the input color `c` is in teardown state.
fn is_pending(value: double_reg_type, c: color) bool {
  const color_id = @get_int(c);
  const color_bit = @as(double_reg_type, 1) << color_id;
  return (value & color_bit) != 0;
}

// Exit the teardown state for a given color `c`.
fn exit(c: color) void {
  const id = @get_int(c);
  const offset = @as(reg_type, addresses.COLOR_CONFIG) + id * word_size;
  const mask = 1 << target.teardown_in_progress_offset;
  @bitcast(reg_ptr, offset).* ^= mask;
}
