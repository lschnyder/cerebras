
import numpy as np

B = 4
PEs = 3

inputs = np.arange(B*PEs, dtype=np.float32)

print(inputs)
print(inputs.reshape(PEs, B))

expected_res = np.flip(inputs.reshape(PEs, B), 0)

print(np.flip(inputs.reshape(PEs, B), 0).flatten())