#!/usr/bin/env bash

set -e

for num_pes in {2..22}
do
    for vec_len in {100..100}
    do
        x_dim=$((7+$num_pes))

        cslc ./layout.csl --fabric-dims=$x_dim,3 \
        --fabric-offsets=4,1 --params=PEs:$num_pes,B:$vec_len -o out --memcpy --channels 1
        cs_python run.py --name out
    done
done