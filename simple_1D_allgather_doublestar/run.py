#!/usr/bin/env cs_python

import argparse
import json
import numpy as np
from cerebras.sdk.runtime.sdkruntimepybind import SdkRuntime, MemcpyDataType, MemcpyOrder # pylint: disable=no-name-in-module

# Read arguments
parser = argparse.ArgumentParser()
parser.add_argument('--name', help="the test compile output dir")
parser.add_argument('--cmaddr', help="IP:port for CS system")
args = parser.parse_args()

# Get matrix dimensions from compile metadata
with open(f"{args.name}/out.json", encoding='utf-8') as json_file:
  compile_data = json.load(json_file)

PEs = int(compile_data['params']['PEs'])
B = int(compile_data['params']['B'])

inputs = np.arange(1, B*PEs + 1, dtype=np.float32)
                                  
runner = SdkRuntime(args.name, cmaddr=args.cmaddr)

b_symbol = runner.get_id('b')
res_symbol = runner.get_id('res')

print("B=", B, " PEs=", PEs)

print("Starting simulation...")

runner.load()
runner.run()

print("simulation successfully started!")
print("copying data to device...")

runner.memcpy_h2d(b_symbol, inputs, 0, 0, PEs, 1, B, streaming=False,
                  order=MemcpyOrder.ROW_MAJOR, data_type=MemcpyDataType.MEMCPY_32BIT, nonblock=False)

print("data successfully copied!")
print("launching...")

runner.launch('execute', nonblock=False)

print("successfully launched!")
print("copying data back...")

res = np.zeros([B*PEs*PEs], dtype=np.float32)
runner.memcpy_d2h(res, res_symbol, 0, 0, PEs, 1, B*PEs, streaming=False,
                  order=MemcpyOrder.ROW_MAJOR, data_type=MemcpyDataType.MEMCPY_32BIT, nonblock=False)

print("data successfully copied!")

runner.stop()

print(res)

for i in range(PEs):
  res_i = res[i*PEs*B:(i+1)*PEs*B]
  np.testing.assert_allclose(inputs, res_i)
print("Brother, I think it worked!")