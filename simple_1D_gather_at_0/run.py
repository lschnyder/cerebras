#!/usr/bin/env cs_python

import argparse
import json
import numpy as np
from cerebras.sdk.runtime.sdkruntimepybind import SdkRuntime, MemcpyDataType, MemcpyOrder # pylint: disable=no-name-in-module

# Read arguments
parser = argparse.ArgumentParser()
parser.add_argument('--name', help="the test compile output dir")
parser.add_argument('--cmaddr', help="IP:port for CS system")
args = parser.parse_args()

# Get matrix dimensions from compile metadata
with open(f"{args.name}/out.json", encoding='utf-8') as json_file:
  compile_data = json.load(json_file)

PEs = int(compile_data['params']['PEs'])
B = int(compile_data['params']['B'])

inputs = np.arange(B*PEs, dtype=np.float32)

result_expected = np.flip(inputs.reshape(PEs, B), 0).flatten()

runner = SdkRuntime(args.name, cmaddr=args.cmaddr)

b_symbol = runner.get_id('b')
result_symbol = runner.get_id('result')

runner.load()
runner.run()

runner.memcpy_h2d(b_symbol, inputs, 0, 0, PEs, 1, B, streaming=False,
                  order=MemcpyOrder.ROW_MAJOR, data_type=MemcpyDataType.MEMCPY_32BIT, nonblock=False)

runner.launch('execute', nonblock=False);

result = np.zeros([B*PEs], dtype=np.float32)
runner.memcpy_d2h(result, result_symbol, PEs-1, 0, 1, 1, B*PEs, streaming=False,
                  order=MemcpyOrder.ROW_MAJOR, data_type=MemcpyDataType.MEMCPY_32BIT, nonblock=False)

runner.stop()

print("Expected result", result_expected)
print("Actual result", result)
np.testing.assert_allclose(result, result_expected)
print("Brother, I think it worked!")