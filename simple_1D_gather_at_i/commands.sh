#!/usr/bin/env bash

set -e

cslc ./layout.csl --fabric-dims=22,3 \
--fabric-offsets=4,1 --params=PEs:15,B:13,i:4 -o out --memcpy --channels 1

cs_python run.py --name out