#!/usr/bin/env bash

set -e

for num_paths in {3..3}
do
    for vec_len in {4..4}
    do
        num_pes=$((2*num_paths))
        x_dim=$((7+$num_pes))
        cslc ./layout.csl --fabric-dims=$x_dim,3 \
        --fabric-offsets=4,1 --params=Pw:$num_pes,B:$vec_len -o out --memcpy --channels 1
        cs_python run.py --name out
    done
done