#!/usr/bin/env bash

set -e

export SINGULARITYENV_SIMFABRIC_DEBUG=inst_trace

cslc ./layout.csl --fabric-dims=10,3 \
--fabric-offsets=4,1 -o out --memcpy --channels 1

cs_python run.py --name out