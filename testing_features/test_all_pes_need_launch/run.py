#!/usr/bin/env cs_python

import argparse
import json
import numpy as np
from cerebras.sdk.runtime.sdkruntimepybind import SdkRuntime, MemcpyDataType, MemcpyOrder # pylint: disable=no-name-in-module

# Read arguments
parser = argparse.ArgumentParser()
parser.add_argument('--name', help="the test compile output dir")
parser.add_argument('--cmaddr', help="IP:port for CS system")
args = parser.parse_args()

# Get matrix dimensions from compile metadata
with open(f"{args.name}/out.json", encoding='utf-8') as json_file:
  compile_data = json.load(json_file)

runner = SdkRuntime(args.name, cmaddr=args.cmaddr)

runner.load()
runner.run()

# runner.memcpy_h2d(b_symbol, inputs, 0, 0, PEs, 1, B, streaming=False,
#                   order=MemcpyOrder.ROW_MAJOR, data_type=MemcpyDataType.MEMCPY_32BIT, nonblock=False)

runner.launch('execute', nonblock=False);

# runner.memcpy_d2h(result, result_symbol, PEs-1, 0, 1, 1, B*PEs, streaming=False,
#                   order=MemcpyOrder.ROW_MAJOR, data_type=MemcpyDataType.MEMCPY_32BIT, nonblock=False)

runner.stop()

print("Brother, I think it worked!")