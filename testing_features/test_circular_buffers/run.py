#!/usr/bin/env cs_python

import argparse
import json
import numpy as np
from cerebras.sdk.runtime.sdkruntimepybind import SdkRuntime, MemcpyDataType, MemcpyOrder # pylint: disable=no-name-in-module

# Read arguments
parser = argparse.ArgumentParser()
parser.add_argument('--name', help="the test compile output dir")
parser.add_argument('--cmaddr', help="IP:port for CS system")
args = parser.parse_args()

# Get matrix dimensions from compile metadata
with open(f"{args.name}/out.json", encoding='utf-8') as json_file:
  compile_data = json.load(json_file)

runner = SdkRuntime(args.name, cmaddr=args.cmaddr)

addr_symbol = runner.get_id('addr')

print("Starting simulation...")

runner.load()
runner.run()

print("simulation successfully started!")
print("copying data to device...")

runner.launch('execute', nonblock=False)

print("successfully launched!")
print("copying data back...")

addr = np.zeros([1], dtype=np.uint32)
runner.memcpy_d2h(addr, addr_symbol, 0, 0, 1, 1, 1, streaming=False,
                  order=MemcpyOrder.ROW_MAJOR, data_type=MemcpyDataType.MEMCPY_32BIT, nonblock=False)

runner.stop()
print(addr)
print("Brother, I think it worked!")