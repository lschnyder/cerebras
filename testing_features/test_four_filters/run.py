#!/usr/bin/env cs_python

import argparse
import json
import numpy as np
from cerebras.sdk.runtime.sdkruntimepybind import SdkRuntime, MemcpyDataType, MemcpyOrder # pylint: disable=no-name-in-module

# Read arguments
parser = argparse.ArgumentParser()
parser.add_argument('--name', help="the test compile output dir")
parser.add_argument('--cmaddr', help="IP:port for CS system")
args = parser.parse_args()

# Get matrix dimensions from compile metadata
with open(f"{args.name}/out.json", encoding='utf-8') as json_file:
  compile_data = json.load(json_file)
                                  
runner = SdkRuntime(args.name, cmaddr=args.cmaddr)

print("Starting simulation...")

runner.load()
runner.run()

print("simulation successfully started!")

runner.launch('execute', nonblock=False)

print("successfully launched!")

runner.stop()

print(res)

# np.testing.assert_allclose(inputs, res)
print("Done")