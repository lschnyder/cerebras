#!/usr/bin/env cs_python

import argparse
import json
import numpy as np
from cerebras.sdk.runtime.sdkruntimepybind import SdkRuntime, MemcpyDataType, MemcpyOrder # pylint: disable=no-name-in-module

# Read arguments
parser = argparse.ArgumentParser()
parser.add_argument('--name', help="the test compile output dir")
parser.add_argument('--cmaddr', help="IP:port for CS system")
args = parser.parse_args()

# Get matrix dimensions from compile metadata
with open(f"{args.name}/out.json", encoding='utf-8') as json_file:
  compile_data = json.load(json_file)

B = int(compile_data['params']['B'])

inputs = np.arange(1, 1 + B*3, dtype=np.float32)

# TODO: Construct expected result based on i
result_expected = np.flip(inputs.reshape(3, B), 0).flatten()

runner = SdkRuntime(args.name, cmaddr=args.cmaddr)

b_symbol = runner.get_id('b')
result_symbol = runner.get_id('res')

print("Starting simulation...")

runner.load()
runner.run()

print("simulation successfully started!")
print("copying data to device...")

runner.memcpy_h2d(b_symbol, inputs, 0, 0, 3, 1, B, streaming=False,
                  order=MemcpyOrder.ROW_MAJOR, data_type=MemcpyDataType.MEMCPY_32BIT, nonblock=False)

print("data successfully copied!")
print("launching...")

runner.launch('execute', nonblock=False);

print("successfully launched!")
print("copying data back...")

result = np.zeros([3*B], dtype=np.float32)
runner.memcpy_d2h(result, result_symbol, 1, 0, 1, 1, 3*B, streaming=False,
  order=MemcpyOrder.ROW_MAJOR, data_type=MemcpyDataType.MEMCPY_32BIT, nonblock=False)

print("data successfully copied!")

runner.stop()

print("Expected result", result_expected)
print("Actual result", result)
np.testing.assert_allclose(result, result_expected)
print("Brother, I think it worked!")