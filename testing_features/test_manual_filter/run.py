#!/usr/bin/env cs_python

import argparse
import json
import numpy as np
from cerebras.sdk.runtime.sdkruntimepybind import SdkRuntime, MemcpyDataType, MemcpyOrder # pylint: disable=no-name-in-module

# Read arguments
parser = argparse.ArgumentParser()
parser.add_argument('--name', help="the test compile output dir")
parser.add_argument('--cmaddr', help="IP:port for CS system")
args = parser.parse_args()

# Get matrix dimensions from compile metadata
with open(f"{args.name}/out.json", encoding='utf-8') as json_file:
  compile_data = json.load(json_file)

B = int(compile_data['params']['B'])

inputs = np.arange(1, B + 1, dtype=np.uint32)
                                  
runner = SdkRuntime(args.name, cmaddr=args.cmaddr)

b_symbol = runner.get_id('b')
res_symbol = runner.get_id('res')

print("B=", B)

print("Starting simulation...")

runner.load()
runner.run()

print("simulation successfully started!")
print("copying data to device...")

runner.memcpy_h2d(b_symbol, inputs, 0, 0, 1, 1, B, streaming=False,
                  order=MemcpyOrder.ROW_MAJOR, data_type=MemcpyDataType.MEMCPY_32BIT, nonblock=False)

print("data successfully copied!")
print("launching...")

runner.launch('execute', nonblock=False)

print("successfully launched!")
print("copying data back...")

res = np.zeros([B], dtype=np.uint32)
runner.memcpy_d2h(res, res_symbol, 1, 0, 2, 1, B//2, streaming=False,
                  order=MemcpyOrder.ROW_MAJOR, data_type=MemcpyDataType.MEMCPY_32BIT, nonblock=False)

print("data successfully copied!")

runner.stop()

print(res)

# np.testing.assert_allclose(inputs, res)
print("Brother, I think it worked!")