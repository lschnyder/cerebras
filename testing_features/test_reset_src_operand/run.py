#!/usr/bin/env cs_python

import argparse
import json
import numpy as np
from cerebras.sdk.runtime.sdkruntimepybind import SdkRuntime, MemcpyDataType, MemcpyOrder # pylint: disable=no-name-in-module

# Read arguments
parser = argparse.ArgumentParser()
parser.add_argument('--name', help="the test compile output dir")
parser.add_argument('--cmaddr', help="IP:port for CS system")
args = parser.parse_args()

# Get matrix dimensions from compile metadata
with open(f"{args.name}/out.json", encoding='utf-8') as json_file:
  compile_data = json.load(json_file)

# B = int(compile_data['params']['B'])

# inputs = np.arange(1, 1 + 2*B, dtype=np.float32)

# TODO: Construct expected result based on i
# result_expected = np.tile(inputs, (1,2)).flatten()

runner = SdkRuntime(args.name, cmaddr=args.cmaddr)

print("Starting simulation...")

runner.load()
runner.run()

print("simulation successfully started!")
print("copying data to device...")

runner.launch('execute', nonblock=False)

print("successfully launched!")
print("copying data back...")

runner.stop()

print("Brother, I think it worked!")