#!/usr/bin/env bash

set -e

export SINGULARITYENV_SIMFABRIC_DEBUG=inst_trace

cslc ./layout.csl --fabric-dims=9,3 \
--fabric-offsets=4,1 --params=B:100 -o out --memcpy --channels 1

cs_python run.py --name out