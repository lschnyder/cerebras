#!/usr/bin/env bash

set -e

# if we use x*y kernels, then 
# --fabric-dims=x+7,y+2
# --fabric-offset=4,1
cslc ./layout.csl --fabric-dims=8,3 \
--fabric-offsets=4,1 -o out --memcpy --channels 1
cs_python run.py --name out
