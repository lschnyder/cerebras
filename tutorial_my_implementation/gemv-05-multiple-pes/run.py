#!/usr/bin/env cs_python

import argparse
import json
import numpy as np

from cerebras.sdk.runtime.sdkruntimepybind import SdkRuntime, MemcpyDataType, MemcpyOrder # pylint: disable=no-name-in-module

# Read arguments
parser = argparse.ArgumentParser()
parser.add_argument('--name', help="the test compile output dir")
parser.add_argument('--cmaddr', help="IP:port for CS system")
args = parser.parse_args()

#HELP: What happens here?
with open(f"{args.name}/out.json", encoding='utf-8') as json_file:
  compile_data = json.load(json_file)


M = int(compile_data['params']['M'])
N = int(compile_data['params']['N'])
width = int(compile_data['params']['width'])

A = np.arange(M*N ,dtype=np.float32)
x = np.full(shape=N, fill_value=1.0, dtype=np.float32)
b = np.full(shape=M, fill_value=2.0, dtype=np.float32)

y_expected = A.reshape(M,N) @ x + b

#REVIEW: Used to write and launch program on device. Better explanation? what is the first argument for?
# first argument is the directory of the ELF files created by the compiler
simulator = SdkRuntime(args.name, cmaddr=args.cmaddr)

y_symbol = simulator.get_id("y")
x_symbol = simulator.get_id("x")
b_symbol = simulator.get_id("b")
A_symbol = simulator.get_id("A")

# This "loads the ELF files"
simulator.load()
# This starts the simulator
simulator.run()

simulator.memcpy_h2d(A_symbol, np.tile(A, width), 0, 0, width, 1, M*N, streaming=False,
                order=MemcpyOrder.ROW_MAJOR, data_type=MemcpyDataType.MEMCPY_32BIT, nonblock=False)

simulator.memcpy_h2d(x_symbol, np.tile(x, width), 0, 0, width, 1, N, streaming=False,
                order=MemcpyOrder.ROW_MAJOR, data_type=MemcpyDataType.MEMCPY_32BIT, nonblock=False)

simulator.memcpy_h2d(b_symbol, np.tile(b, width), 0, 0, width, 1, M, streaming=False,
                order=MemcpyOrder.ROW_MAJOR, data_type=MemcpyDataType.MEMCPY_32BIT, nonblock=False)

#REVIEW: What does nonblock do here?
# I think this means that the below function call returns possibly even before the
# execute function has started.
simulator.launch("execute", nonblock=False)

y_res = np.tile(np.zeros(shape=M, dtype=np.float32), width)
simulator.memcpy_d2h(y_res, y_symbol, 0, 0, width, 1, M, streaming=False,
             order=MemcpyOrder.ROW_MAJOR, data_type=MemcpyDataType.MEMCPY_32BIT, nonblock=False)

simulator.stop()

np.testing.assert_allclose(y_res, np.tile(y_expected, width), atol=0.01, rtol=0.00)
print("CONGRATULATIONS! YOUR PROGRAM MIGHT WORK AS INTENDED!")
