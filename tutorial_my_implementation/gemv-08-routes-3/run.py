#!/usr/bin/env cs_python

import argparse
import json
import numpy as np

from cerebras.sdk.runtime.sdkruntimepybind import SdkRuntime, MemcpyDataType, MemcpyOrder # pylint: disable=no-name-in-module

# Read arguments
parser = argparse.ArgumentParser()
parser.add_argument('--name', help="the test compile output dir")
parser.add_argument('--cmaddr', help="IP:port for CS system")
args = parser.parse_args()

#HELP: What happens here?
with open(f"{args.name}/out.json", encoding='utf-8') as json_file:
  compile_data = json.load(json_file)


M = int(compile_data['params']['M'])
N = int(compile_data['params']['N'])
kernel_x_dim = int(compile_data['params']['kernel_x_dim'])
kernel_y_dim = int(compile_data['params']['kernel_y_dim'])

M_per_PE = M // kernel_y_dim;
N_per_PE = N // kernel_x_dim;

A = np.arange(M*N ,dtype=np.float32).reshape(M,N)
x = np.full(shape=N, fill_value=1.0, dtype=np.float32)
b = np.full(shape=M, fill_value=2.0, dtype=np.float32)

y_expected = A @ x + b

#REVIEW: Used to write and launch program on device. Better explanation? what is the first argument for?
# first argument is the directory of the ELF files created by the compiler
simulator = SdkRuntime(args.name, cmaddr=args.cmaddr)

y_symbol = simulator.get_id("y")
x_symbol = simulator.get_id("x")
A_symbol = simulator.get_id("A")
# This "loads the ELF files"
print("starting simulator")
simulator.load()
# This starts the simulator
simulator.run()
print("simulator started")
print("copying data...")
A_prepared = A.reshape(kernel_y_dim, M_per_PE, kernel_x_dim, N_per_PE).transpose(0, 2, 3, 1).ravel()
simulator.memcpy_h2d(A_symbol, A_prepared, 0, 0, kernel_x_dim, kernel_y_dim, M_per_PE*N_per_PE, streaming=False,
                order=MemcpyOrder.ROW_MAJOR, data_type=MemcpyDataType.MEMCPY_32BIT, nonblock=False)

simulator.memcpy_h2d(x_symbol, x, 0, 0, kernel_x_dim, 1, N_per_PE, streaming=False,
                order=MemcpyOrder.ROW_MAJOR, data_type=MemcpyDataType.MEMCPY_32BIT, nonblock=False)

simulator.memcpy_h2d(y_symbol, b, 0, 0, 1, kernel_y_dim, M_per_PE, streaming=False,
                order=MemcpyOrder.ROW_MAJOR, data_type=MemcpyDataType.MEMCPY_32BIT, nonblock=False)

print("data copied")
#REVIEW: What does nonblock do here?
# I think this means that the below function call returns possibly even before the
# execute function has started.
simulator.launch("execute", nonblock=False)
print("executing")
y_res = np.zeros(shape=M, dtype=np.float32)
simulator.memcpy_d2h(y_res, y_symbol, kernel_x_dim-1, 0, 1, kernel_y_dim, M_per_PE, streaming=False,
             order=MemcpyOrder.ROW_MAJOR, data_type=MemcpyDataType.MEMCPY_32BIT, nonblock=False)

simulator.stop()

np.testing.assert_allclose(y_res, y_expected, atol=0.01, rtol=0.00)
print("CONGRATULATIONS! YOUR PROGRAM MIGHT WORK AS INTENDED!")
